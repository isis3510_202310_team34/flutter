import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

// ignore: constant_identifier_names
enum RestaurantTags { Vegetarian, Italian, Mexican, Burgers, Pizza }

extension ParseToString on RestaurantTags {
  String toShortString() {
    return toString().split('.').last;
  }
}

class RestaurantModel {
  final String? uid;
  final String name;
  final String image;
  final GeoPoint? location;
  final List<RestaurantTags> tags;

  const RestaurantModel(
      {this.uid,
      required this.name,
      required this.image,
      this.location,
      required this.tags});

  factory RestaurantModel.fromJson(DocumentSnapshot doc) {
    Map data = doc.data() as Map;

    List<String> stringTags = List<String>.from(data['tags']);
    List<RestaurantTags> tags =
        RestaurantModel.stringListToRestaurantTagsList(stringTags);

    return RestaurantModel(
        uid: doc.id,
        name: data['name'],
        image: data['image'],
        location: data['location'],
        tags: tags);
  }

  Map<String, dynamic> toJson() {
    return {'name': name, 'image': image, 'location': location, "tags": tags};
  }

  static List<String> restaurantTagsListToStringsList(
      List<RestaurantTags> tags) {
    List<String> stringTags = [];

    for (int i = 0; i < tags.length; i++) {
      RestaurantTags tag = tags[i];
      stringTags.add(tag.toShortString());
    }

    stringTags.sort();

    return stringTags;
  }

  static List<RestaurantTags> stringListToRestaurantTagsList(
      List<String> stringTags) {
    List<RestaurantTags> tags = [];

    for (int i = 0; i < stringTags.length; i++) {
      String string = stringTags[i];

      for (RestaurantTags tag in RestaurantTags.values) {
        if (tag.toShortString() == string) {
          tags.add(tag);
          break;
        }
      }
    }
    return tags;
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'name': name,
      'image': image,
      'latitude': location?.latitude,
      'longitude': location?.longitude,
      'tags': jsonEncode(RestaurantModel.restaurantTagsListToStringsList(tags)),
    };
  }
}
