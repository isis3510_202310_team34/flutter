class DishModel {
  String? uid;
  String? name;
  int? price;
  String? description;
  String? image;

  DishModel({this.uid, this.name, this.price, this.description, this.image});

  factory DishModel.fromJson(Map<String, dynamic> json) {
    return DishModel(
      uid: json['uid'],
      name: json['name'],
      price: int.parse(json['price']),
      description: json['description'],
      image: json['image'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'price': price,
      'description': description,
      'image': image,
    };
  }
}
