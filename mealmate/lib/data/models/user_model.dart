import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {
  final String? uid;
  final String name;
  final String email;
  final String role;
  final String phone;
  final List<String> favoriteRestaurants;
  final List<String> preferences;
  final String restaurant;

  const UserModel({
    this.uid,
    required this.name,
    required this.email,
    required this.role,
    required this.phone,
    this.restaurant = "",
    this.favoriteRestaurants = const [],
    this.preferences = const [],
  });

  factory UserModel.fromJson(DocumentSnapshot doc) {
    Map data = doc.data() as Map;
    return UserModel(
      uid: doc.id,
      name: data['name'] ?? '',
      email: data['email'] ?? '',
      role: data['role'] ?? '',
      phone: data['phone'] ?? '',
      restaurant: data['restaurant'] ?? '',
      favoriteRestaurants: data['favoriteRestaurants'] ?? [],
      preferences: data['preferences'] ?? [],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'email': email,
      'role': role,
      'phone': phone,
      'restaurant': restaurant,
      'favoriteRestaurants': favoriteRestaurants,
      'preferences': preferences,
    };
  }

  // Is Owner
  bool get isOwner => role == 'owner';
}
