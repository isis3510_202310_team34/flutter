class PromotionModel {
  String? uid;
  String dishName;
  String restaurantName;
  double newPrices;
  String image;
  double discountPercentage;
  String description;
  double currentlyPrice;
  bool active;

  PromotionModel(
      {this.uid,
      required this.dishName,
      required this.restaurantName,
      required this.newPrices,
      required this.image,
      required this.discountPercentage,
      required this.description,
      required this.currentlyPrice,
      required this.active});

  factory PromotionModel.fromJson(Map<String, dynamic> json) {
    return PromotionModel(
      dishName: json['dishName'],
      restaurantName: json["restaurantName"],
      newPrices: double.parse(json['newPrices']),
      image: json["image"],
      discountPercentage: double.parse(json['discountPercentage']),
      description: json["description"],
      currentlyPrice: double.parse(json['currentlyPrice']),
      active: true,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      "dishName": dishName,
      "restaurantName": restaurantName,
      "newPrices": newPrices,
      "image": image,
      "discountPercentage": discountPercentage,
      "description": description,
      "currentlyPrice": currentlyPrice,
      "active": active
    };
  }
}
