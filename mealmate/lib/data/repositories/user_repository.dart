import 'package:mealmate/data/data_providers/user_api.dart';

class UserRepository {
  final UserApi userApi = UserApi();

  Future<Map<String, dynamic>?> getUserData(String userId) async {
    return await userApi.getUserData(userId);
  }

  // Add a restaurant to the user's favorite list, if the restaurant is already in the list, remove it
  Future<void> toggleFavorite(String userId, String restaurantId) async {
    final data = await userApi.getUserData(userId);

    List<String> favoriteRestaurants = [];

    if (data['favorite_restaurants'] != null) {
      List<dynamic> dynamicList = data['favorite_restaurants'];
      for (int i = 0; i < dynamicList.length; i++) {
        favoriteRestaurants.add(dynamicList[i] as String);
      }
    }

    if (favoriteRestaurants.contains(restaurantId)) {
      favoriteRestaurants.remove(restaurantId);
    } else {
      favoriteRestaurants.add(restaurantId);
    }

    await userApi.updateFavoriteRestaurants(userId, favoriteRestaurants);
  }

  Future<List<String>> getFavoriteRestaurants(String userId) async {
    final data = await userApi.getUserData(userId);

    if (data.containsKey("favorite_restaurants")) {
      List<dynamic> favoriteRestaurants = data['favorite_restaurants'];
      List<String> restaurantList = [];

      for (int i = 0; i < favoriteRestaurants.length; i++) {
        String restaurant = favoriteRestaurants[i].toString();
        restaurantList.add(restaurant);
      }

      return restaurantList;
    } else {
      return [];
    }
  }

  Future<Map<String, dynamic>> getUser(String userId) async {
    return await userApi.getUserData(userId);
  }

  Future<void> addRestaurantToOwner({
    required String ownerId,
    required String restaurantId,
  }) async {
    await userApi.addRestaurantToOwner(
        ownerId: ownerId, restaurantId: restaurantId);
  }

  Future<String> getCurrentUserId() async {
    return await userApi.getCurrentUserId();
  }
}
