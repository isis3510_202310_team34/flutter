import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:mealmate/data/data_providers/dishes_api.dart';
import 'package:mealmate/data/models/dish_model.dart';

import '../../business_logic/exceptions/exceptions.dart';

class DishRepository {
  final DishesApi dishApi = DishesApi();

  Future<String> addDishToRestaurant({
    required String name,
    required int price,
    required String description,
    required String image,
    required String restaurantIdN,
  }) async {
    final dish = DishModel(
        name: name, price: price, description: description, image: image);

    Map<String, dynamic> dishJson = dish.toMap();

    String dishId = await dishApi.addDishToRestaurant(
        dishJson: dishJson, restaurantId: restaurantIdN);

    return dishId;
  }

  Future<List<DishModel>> getDishesByRestaurantId(
      {required String restaurantId}) async {
    ConnectivityResult connectivityResult =
        await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      throw NoInternetConnectionException();
    }
    QuerySnapshot<Map<String, dynamic>> dishes =
        await dishApi.getDishesByRestaurantId(restaurantId);
    List<DishModel> listDishes = [];
    dishes.docs.forEach((element) {
      print(element.data());
      String uid = element.id;
      String name = element.data()['name'] ?? '';
      int price = element.data()['price'] ?? 0;
      String description = element.data()['description'] ?? '';
      String image = element.data()['image'] ?? '';
      listDishes.add(DishModel(
          uid: uid,
          name: name,
          price: price,
          description: description,
          image: image));
    });
    return listDishes;
  }
}
