import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:mealmate/data/models/promotion_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../../business_logic/exceptions/exceptions.dart';
import '../data_providers/promotion_api.dart';

class PromotionRepository {
  final PromotionsApi promotionsApi = PromotionsApi();
  late Database database;

  PromotionRepository() {
    initializeDatabase();
  }

  Future<void> initializeDatabase() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, 'promotion.db');
    database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
      CREATE TABLE IF NOT EXISTS promotion (
        id INTEGER PRIMARY KEY,
        uid TEXT,
        dishName TEXT,
        restaurantName TEXT,
        newPrices REAL,
        image TEXT,
        discountPercentage REAL,
        description TEXT,
        currentlyPrice REAL,
        active INTEGER,
        createdDate TEXT
      )
    ''');
    });
  }

  Future<String> addPromotionToRestaurant(
      {required String dishName,
      required String restaurantName,
      required String description,
      required String image,
      required double currentlyPrice,
      required double newPrices,
      required double discountPercentage,
      required String restaurantIdN}) async {
    await database.delete('promotion');
    final promotion = PromotionModel(
        dishName: dishName,
        restaurantName: restaurantName,
        image: image,
        currentlyPrice: currentlyPrice,
        newPrices: newPrices,
        discountPercentage: discountPercentage,
        description: description,
        active: true);

    Map<String, dynamic> promotionJson = promotion.toMap();

    String promotionId = await promotionsApi.addPromotionToRestaurant(
        promotionJson: promotionJson, restaurantId: restaurantIdN);

    return promotionId;
  }

  Future<List<PromotionModel>> getPromotionsByRestaurantId(
      {required String restaurantId}) async {
    QuerySnapshot<Map<String, dynamic>> promotions =
        await promotionsApi.getPromotionsByRestaurantId(restaurantId);
    List<PromotionModel> listPromotions = [];
    for (var element in promotions.docs) {
      print(element.data());
      String uid = element.id;
      String dishName = element.data()['dishName'] ?? '';
      String restaurantName = element.data()['restaurantName'] ?? "";
      double newPrices = element.data()['newPrices'].toDouble() ?? 0.0;
      String image = element.data()['image'] ?? '';
      double discountPercentage =
          element.data()['discountPercentage'].toDouble() ?? 0.0;
      String description = element.data()['description'] ?? "";
      double currentlyPrice =
          element.data()['currentlyPrice'].toDouble() ?? 0.0;
      bool active = element.data()['active'] ?? false;
      listPromotions.add(PromotionModel(
          uid: uid,
          dishName: dishName,
          restaurantName: restaurantName,
          newPrices: newPrices,
          image: image,
          discountPercentage: discountPercentage,
          description: description,
          currentlyPrice: currentlyPrice,
          active: active));
    }
    return listPromotions;
  }

  Future<List<PromotionModel>> getAllPromotions() async {
    final bool isCacheExpired = await _isCacheExpired();
    if (isCacheExpired) {
      QuerySnapshot<Map<String, dynamic>> promotionsSnapshot =
          await promotionsApi.getAllPromotions();
      List<PromotionModel> allPromotions =
          await _docSnapshotToPromotions(promotionsSnapshot);

      await _storePromotionsInCache(allPromotions);
      return allPromotions;
    } else {
      return await getAllCachedPromotions();
    }
  }

  Future<bool> _isCacheExpired() async {
    final List<Map<String, dynamic>> cachedPromotions =
        await database.query('promotion');
    if (cachedPromotions.isNotEmpty) {
      final DateTime createdDate = DateTime.parse(cachedPromotions[0]
          ['createdDate'] as String); // Parse the stored createdDate
      final DateTime now = DateTime.now();
      final Duration difference = now.difference(createdDate);

      const int maxCacheDuration = 10 * 60; // 10 minutes in seconds
      return difference.inSeconds > maxCacheDuration;
    }
    // If no cached promotions found, consider cache expired
    return true;
  }

  Future<void> _storePromotionsInCache(List<PromotionModel> promotions) async {
    // Clear the existing cached promotions
    await database.delete('promotion');

    // Store each promotion in the cache with its createdDate
    final DateTime now = DateTime.now();
    final String createdDate = now.toIso8601String();

    for (var promotion in promotions) {
      final Map<String, dynamic> promotionMap = promotion.toMap();
      promotionMap['createdDate'] =
          createdDate; // Add the createdDate to the promotion data
      await database.insert('promotion', promotionMap);
      print("Saved a promotion to cache");
    }
  }

  Future<List<PromotionModel>> getAllCachedPromotions() async {
    final List<Map<String, dynamic>> result = await database.query('promotion');

    if (result.isNotEmpty) {
      return await compute(_deserializePromotions, result);
    }

    throw NoCachedDataException();
  }

  static List<PromotionModel> _deserializePromotions(
      List<Map<String, dynamic>> promotions) {
    List<PromotionModel> deserializedPromotions = [];

    for (int i = 0; i < promotions.length; i++) {
      Map<String, dynamic> row = promotions[i];

      bool active =
          row['active'] == 1 ? true : false; // Conversión de int a bool

      PromotionModel promotion = PromotionModel(
        uid: row['uid'],
        dishName: row['dishName'],
        restaurantName: row['restaurantName'],
        newPrices: row['newPrices'],
        image: row['image'],
        discountPercentage: row['discountPercentage'],
        description: row['description'],
        currentlyPrice: row['currentlyPrice'],
        active: active,
      );

      deserializedPromotions.add(promotion);
    }

    return deserializedPromotions;
  }

  static Future<List<PromotionModel>> _docSnapshotToPromotions(
    QuerySnapshot<Map<String, dynamic>> restaurantsSnapshot,
  ) async {
    List<PromotionModel> allPromotions = [];
    for (var restaurantDoc in restaurantsSnapshot.docs) {
      QuerySnapshot<Map<String, dynamic>> promotionsSnapshot =
          await restaurantDoc.reference.collection('promotions').get();
      for (var promotionDoc in promotionsSnapshot.docs) {
        Map<String, dynamic> promotionData = promotionDoc.data();
        String uid = promotionDoc.id;
        String dishName = promotionData['dishName'] ?? '';
        String restaurantName = promotionData['restaurantName'] ?? "";
        double newPrices = promotionData['newPrices'].toDouble() ?? 0.0;
        String image = promotionData['image'] ?? '';
        double discountPercentage =
            promotionData['discountPercentage'].toDouble() ?? 0.0;
        String description = promotionData['description'] ?? "";
        double currentlyPrice =
            promotionData['currentlyPrice'].toDouble() ?? 0.0;
        bool active = promotionData['active'] ?? false;
        allPromotions.add(PromotionModel(
            uid: uid,
            dishName: dishName,
            restaurantName: restaurantName,
            newPrices: newPrices,
            image: image,
            discountPercentage: discountPercentage,
            description: description,
            currentlyPrice: currentlyPrice,
            active: active));
      }
    }
    return allPromotions;
  }
}
