import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:mealmate/business_logic/exceptions/exceptions.dart';
import 'package:mealmate/data/data_providers/restaurant_api.dart';
import 'package:mealmate/data/data_providers/user_api.dart';
import 'package:mealmate/data/models/restaurant_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class RestaurantRepository {
  final RestaurantApi restaurantApi = RestaurantApi();
  final UserApi userApi = UserApi();
  late Database database;

  RestaurantRepository() {
    _initializeDatabase();
  }

  Future<void> _initializeDatabase() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, 'restaurant.db');
    database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      // Create the tables or perform any other initialization tasks
      await db.execute('''
  CREATE TABLE IF NOT EXISTS restaurant (
    id INTEGER PRIMARY KEY,
    uid TEXT,
    name TEXT,
    image TEXT,
    latitude REAL,
    longitude REAL,
    tags BLOB
  )
''');
    });
  }

  Future<String> addRestaurant({
    required String name,
    required String image,
    GeoPoint? location,
    required List<RestaurantTags> tags,
    required String currentUserId,
  }) async {
    final restaurant = RestaurantModel(
        name: name,
        image: image,
        location: const GeoPoint(4.603339, -74.06607),
        tags: tags);

    Map<String, dynamic> restaurantJson = restaurant.toJson();
    List<dynamic> jsonTags = restaurantJson['tags'];

    List<dynamic> newTags = jsonTags
        .map((tag) => tag.toString().replaceAll('RestaurantTags.', ''))
        .toList();
    restaurantJson['tags'] = newTags;

    String restaurantId =
        await restaurantApi.addRestaurant(restaurantJson: restaurantJson);

    await userApi.addRestaurantToOwner(
        ownerId: currentUserId, restaurantId: restaurantId);

    return restaurantId;
  }

  Future<void> deleteRestaurant(String id) async {
    await restaurantApi.deleteRestaurant(id);
  }

  Future<void> updateRestaurant({
    required String uid,
    required String name,
    required String image,
    GeoPoint? location,
    required List<String> tags,
  }) async {
    final restaurant =
        RestaurantModel(name: name, image: image, location: location, tags: []);
    await restaurantApi.updateRestaurant(
        id: uid, restaurantJson: restaurant.toJson());
  }

  //Get all restaurants
  Future<List<RestaurantModel>> getAllRestaurants() async {
    List<RestaurantModel> listAll = [];
    try {
      final restaurants = await restaurantApi.getAllRestaurants();

      listAll = await compute<List<QueryDocumentSnapshot<Map<String, dynamic>>>,
          List<RestaurantModel>>(_docSnapshotToRestaurants, restaurants);
    } catch (e) {
      print(e.toString());
      print('Error getting all restaurants');
    }
    return listAll;
  }

  Future<GetFilteredRestaurantsResult> getFilteredRestaurants(
      List<RestaurantTags> tags) async {
    if (tags.isNotEmpty) {
      try {
        final List<QueryDocumentSnapshot<Map<String, dynamic>>> restaurants =
            await restaurantApi.getFilteredRestaurants(tags);

        List<RestaurantModel> filteredRestaurants = [];

        filteredRestaurants = await compute<
            List<QueryDocumentSnapshot<Map<String, dynamic>>>,
            List<RestaurantModel>>(_docSnapshotToRestaurants, restaurants);

        // Store the filtered restaurants in the cache (SQLite database)
        await _storeFilteredRestaurantsInCache(filteredRestaurants);

        return GetFilteredRestaurantsResult(
            restaurants: filteredRestaurants, isCachedData: false);
      } on NoInternetConnectionException {
        // If there is no internet connection, try to fetch data from the cache
        List<RestaurantModel> cachedRestaurants =
            await _getCachedFilteredRestaurants(tags);

        if (cachedRestaurants.isNotEmpty) {
          // Return the cached data to the caller
          return GetFilteredRestaurantsResult(
              restaurants: cachedRestaurants, isCachedData: true);
        } else {
          // If there is no cached data, throw an exception
          throw NoCachedDataException();
        }
      }
    } else {
      List<RestaurantModel> allRestaurants = await getAllRestaurants();
      return GetFilteredRestaurantsResult(
          restaurants: allRestaurants, isCachedData: false);
    }
  }

  Future<void> _storeFilteredRestaurantsInCache(
      List<RestaurantModel> restaurants) async {
    // Clear the existing cached restaurants
    await database.delete('restaurant');

    // Store each restaurant in the cache
    for (var restaurant in restaurants) {
      await database.insert('restaurant', restaurant.toMap());
      print("Saved a restaurant to cache");
    }
  }

  Future<List<RestaurantModel>> _getCachedFilteredRestaurants(
      List<RestaurantTags> tags) async {
    final List<Map<String, dynamic>> result = await database.query(
      'restaurant',
      where: 'tags LIKE ?',
      whereArgs: [
        '%${jsonEncode(RestaurantModel.restaurantTagsListToStringsList(tags))}%'
      ],
    );

    if (result.isNotEmpty) {
      print("Restaurants from cache:");
      print(result);
      return await compute(_deserializeRestaurants, result);
    }

    throw NoCachedDataException();
  }

  static List<RestaurantModel> _deserializeRestaurants(
      List<Map<String, dynamic>> restaurants) {
    List<RestaurantModel> deserializedRestaurants = [];

    for (int i = 0; i < restaurants.length; i++) {
      Map<String, dynamic> row = restaurants[i];

      RestaurantModel restaurant = RestaurantModel(
        uid: row['uid'],
        name: row['name'],
        image: row['image'],
        location: GeoPoint(row['latitude'], row['longitude']),
        tags: RestaurantModel.stringListToRestaurantTagsList(
          jsonDecode(row['tags']).cast<String>(),
        ),
      );

      deserializedRestaurants.add(restaurant);
    }

    return deserializedRestaurants;
  }

  static List<RestaurantModel> _docSnapshotToRestaurants(
      List<QueryDocumentSnapshot<Map<String, dynamic>>> restaurants) {
    List<RestaurantModel> allRestaurants = [];

    for (var docSnapshot in restaurants) {
      String id = docSnapshot.id;

      String name = docSnapshot.data()['name'] ?? '';

      String image = docSnapshot.data()['image'] ?? '';

      GeoPoint? location = docSnapshot.data()['location'];

      List<String> stringTags = [];
      if (docSnapshot.data()['tags'] != null &&
          docSnapshot.data()['tags'].length > 0) {
        stringTags = List<String>.from(docSnapshot.data()['tags'] ?? []);
      }

      List<RestaurantTags> tags = [];
      for (int i = 0; i < stringTags.length; i++) {
        String string = stringTags[i];
        for (RestaurantTags tag in RestaurantTags.values) {
          if ((tag).toString().split('.').last == string) {
            tags.add(tag);
            break;
          }
        }
      }

      RestaurantModel restaurant = RestaurantModel(
          uid: id, name: name, image: image, location: location, tags: tags);
      allRestaurants.add(restaurant);
    }
    return allRestaurants;
  }

  //Get restaurant by id
  Future<RestaurantModel> getRestaurantById(String id) async {
    final restaurant = await restaurantApi.getRestaurantById(id);
    return RestaurantModel.fromJson(restaurant);
  }

  Future<RestaurantModel> getRestaurantByUserId(String userId) async {
    final doc = await restaurantApi.getRestaurantByUserId(userId);
    return RestaurantModel.fromJson(doc);
  }
}

class GetFilteredRestaurantsResult {
  final List<RestaurantModel> restaurants;
  final bool isCachedData;

  GetFilteredRestaurantsResult({
    required this.restaurants,
    required this.isCachedData,
  });
}
