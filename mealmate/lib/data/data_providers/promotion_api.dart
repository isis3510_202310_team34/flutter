import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import '../../business_logic/exceptions/exceptions.dart';

class PromotionsApi {
  final db = FirebaseFirestore.instance;

  Future<String> addPromotionToRestaurant(
      {required Map<String, dynamic> promotionJson,
      required String restaurantId}) async {
    ConnectivityResult connectivityResult =
        await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      throw NoInternetConnectionException();
    }
    DocumentReference documentReference = await db
        .collection('restaurants')
        .doc(restaurantId)
        .collection("promotions")
        .add(promotionJson);
    return documentReference.id;
  }

  Future<QuerySnapshot<Map<String, dynamic>>> getPromotionsByRestaurantId(
      String restaurantId) async {
    QuerySnapshot<Map<String, dynamic>> listPromotions = await db
        .collection('restaurants')
        .doc(restaurantId)
        .collection("promotions")
        .get();
    return listPromotions;
  }

  Future<QuerySnapshot<Map<String, dynamic>>> getAllPromotions() async {
    ConnectivityResult connectivityResult =
        await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      throw NoInternetConnectionException();
    }
    QuerySnapshot<Map<String, dynamic>> listPromotions =
        await db.collection('restaurants').get();
    return listPromotions;
  }
}
