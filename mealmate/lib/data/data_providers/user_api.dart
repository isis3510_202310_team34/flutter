import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserApi {
  final db = FirebaseFirestore.instance;

  Future<Map<String, dynamic>> getUserData(String userId) async {
    final doc = await db.collection('users').doc(userId).get();
    final data = doc.data();
    print("esto recibimos del user");
    print(data.toString());
    return data!;
  }

  Future<DocumentSnapshot<Map<String, dynamic>>> getUser(String userId) async {
    return await db.collection('users').doc(userId).get();
  }

  Future<void> updateFavoriteRestaurants(
      String userId, List<String> newFavoriteRestaurants) async {
    await db.collection('users').doc(userId).update({
      'favorite_restaurants': newFavoriteRestaurants,
    });
  }

  // Get the list of favorite restaurants
  //Future<List<String>> getFavoriteRestaurants(String userId) async {
  //  final doc = await db.collection('users').doc(userId).get();
  //  final data = doc.data();

  //  return data!.containsKey("favorite_restaurants")
  //      ? (data['favorite_restaurants'] as List<dynamic>)
  //          .map((element) => element.toString())
  //          .toList()
  //      : [];

  //return (data!['favorite_restaurants'] as List)
  //    .map((e) => e as String)
  //    .toList();
  //}

  //Add restaurant to owner
  Future<void> addRestaurantToOwner({
    required String ownerId,
    required String restaurantId,
  }) async {
    await db
        .collection('users')
        .doc(ownerId)
        .update({'restaurant': restaurantId});
  }

  Future<String> getCurrentUserId() async {
    return FirebaseAuth.instance.currentUser!.uid;
  }
}
