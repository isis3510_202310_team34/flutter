import 'package:cloud_firestore/cloud_firestore.dart';

class DishesApi {
  final db = FirebaseFirestore.instance;

  Future<String> addDishToRestaurant(
      {required Map<String, dynamic> dishJson,
      required String restaurantId}) async {
    DocumentReference documentReference = await db
        .collection('restaurants')
        .doc(restaurantId)
        .collection("dishes")
        .add(dishJson);
    return documentReference.id;
  }

  Future<QuerySnapshot<Map<String, dynamic>>> getDishesByRestaurantId(
      String restaurantId) async {
    QuerySnapshot<Map<String, dynamic>> listDishes = await db
        .collection('restaurants')
        .doc(restaurantId)
        .collection("dishes")
        .get();
    return listDishes;
  }
}
