import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import '../../business_logic/exceptions/exceptions.dart';
import '../models/restaurant_model.dart';

class RestaurantApi {
  final db = FirebaseFirestore.instance;

  Future<String> addRestaurant({
    required Map<String, dynamic> restaurantJson,
  }) async {
    DocumentReference documentReference =
        await db.collection('restaurants').add(restaurantJson);
    return documentReference.id;
  }

  Future<void> deleteRestaurant(String id) async {
    await db.collection('restaurants').doc(id).delete();
  }

  Future<void> updateRestaurant({
    required String id,
    required Map<String, dynamic> restaurantJson,
  }) async {
    await db.collection('restaurants').doc(id).update(restaurantJson);
  }

  //Get all restaurants
  Future<List<QueryDocumentSnapshot<Map<String, dynamic>>>>
      getAllRestaurants() async {
    final restaurants = await db.collection('restaurants').get();
    final restaurantsList = restaurants.docs;
    return restaurantsList;
  }

  //Get filtered restaurants
  Future<List<QueryDocumentSnapshot<Map<String, dynamic>>>>
      getFilteredRestaurants(List<RestaurantTags> tags) async {
    // Check internet connection
    ConnectivityResult connectivityResult =
        await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      throw NoInternetConnectionException();
    }
    List<String> stringTags =
        RestaurantModel.restaurantTagsListToStringsList(tags);
    QuerySnapshot<Map<String, dynamic>> querySnapshot = await db
        .collection('restaurants')
        .where('tags',
            arrayContainsAny:
                stringTags) // Buscamos restaurantes que contengan al menos uno de los tags
        .get();

    List<QueryDocumentSnapshot<Map<String, dynamic>>> filteredRestaurants = [];

    for (int i = 0; i < querySnapshot.docs.length; i++) {
      QueryDocumentSnapshot<Map<String, dynamic>> document =
          querySnapshot.docs[i];

      // Filtramos solo los restaurantes que tengan exactamente los filtros que queremos
      List<dynamic> documentTags = document['tags'];
      Set<String> documentTagsSet = {};
      for (int j = 0; j < documentTags.length; j++) {
        documentTagsSet.add(documentTags[j]);
      }

      bool allSelectedTagsInDocument = true;
      for (int j = 0; j < stringTags.length; j++) {
        String tag = stringTags[j];
        if (!documentTagsSet.contains(tag)) {
          allSelectedTagsInDocument = false;
          break;
        }
      }

      if (allSelectedTagsInDocument) {
        filteredRestaurants.add(document);
      }
    }

    return filteredRestaurants;
  }

  //Get restaurant by id
  Future<DocumentSnapshot<Map<String, dynamic>>> getRestaurantById(
      String id) async {
    if (id.isEmpty) {
      throw Exception('Id is empty');
    }
    final restaurant = await db.collection('restaurants').doc(id).get();
    return restaurant;
  }

  Future<DocumentSnapshot<Map<String, dynamic>>> getRestaurantByUserId(
      String userId) async {
    final doc = await db.collection('users').doc(userId).get();
    final data = doc.data();
    final restaurant =
        await db.collection('restaurants').doc(data!["restaurant"]).get();
    return restaurant;
  }

  // Check if restaurant exists
  Future<bool> restaurantExists(String id) async {
    if (id.isEmpty) {
      return false;
    }
    final restaurant = await db.collection('restaurants').doc(id).get();
    return restaurant.exists;
  }
}
