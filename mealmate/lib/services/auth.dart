import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mealmate/data/repositories/restaurant_repository.dart';
import 'package:mealmate/services/sharedPreferences.dart';

import '../../../data/models/user_model.dart';
import '../data/models/restaurant_model.dart';

class Auth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final db = FirebaseFirestore.instance;

  User? get currentUser => _firebaseAuth.currentUser;

  Stream<User?> get authStateChanges => _firebaseAuth.authStateChanges();

  Future<void> signInWithEmailAndPassword(
      {required String email, required String password}) async {
    await _firebaseAuth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
    UserModel currentUser = await Auth().getUser();
    String currentUserId = currentUser.uid!;
    SharedPrefUtils.saveStr("actualId", currentUserId);
    if (currentUser.restaurant != "") {
      SharedPrefUtils.saveStr("actualRestaurantId", currentUser.restaurant);
      RestaurantModel restaurant = await RestaurantRepository()
          .getRestaurantById(currentUser.restaurant);
      SharedPrefUtils.saveStr("actualRestaurantName", restaurant.name);
    }
  }

  Future<void> createUserWithEmailAndPassword(
      {required String email, required String password}) async {
    await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

// Create user, add to Firestore and then createCustomerWithEmailAndPassword
  Future<void> createUser({
    required String name,
    required String email,
    required String password,
    required String role,
    required String phone,
  }) async {
    await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
    var customer = {
      'name': name,
      'email': email,
      'role': role,
      'phone': phone,
      'favorite_restaurants': [],
    };
    var owner = {
      'name': name,
      'email': email,
      'role': role,
      'phone': phone,
      'restaurant': "",
    };
    if (role == "customer") {
      await db
          .collection('users')
          .doc(_firebaseAuth.currentUser!.uid)
          .set(customer);
    } else if (role == "owner") {
      await db
          .collection('users')
          .doc(_firebaseAuth.currentUser!.uid)
          .set(owner);
    }
  }

  Future<void> createOwner({
    required String name,
    required String email,
    required String password,
    required String role,
    required String phone,
    required String restaurant,
    List<String>? favoriteRestaurants,
  }) async {
    await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
    await db.collection('users').doc(_firebaseAuth.currentUser!.uid).set({
      'name': name,
      'email': email,
      'role': role,
      'phone': phone,
      'restaurant': restaurant
    });
  }

  Future<UserModel> getUser() async {
    final user = _firebaseAuth.currentUser;
    print("user:");
    print(user);

    final doc = await db.collection('users').doc(user!.uid).get();
    final data = doc.data();
    print(doc);
    print(doc.data());
    print(doc.data()?['name']);
    return UserModel(
      uid: user.uid,
      name: data!['name'],
      email: data['email'],
      role: data['role'],
      phone: data['phone'],
      favoriteRestaurants: data.containsKey("favorite_restaurants")
          ? (data['favorite_restaurants'] as List<dynamic>)
              .map((element) => element.toString())
              .toList()
          : [],
      preferences: data.containsKey("preferences")
          ? (data['preferences'] as List<dynamic>)
              .map((element) => element.toString())
              .toList()
          : [],
      restaurant:
          data.containsKey("restaurant") ? data['restaurant'].toString() : "",
    );
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }
}
