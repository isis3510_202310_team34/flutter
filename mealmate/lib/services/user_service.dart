import 'package:cloud_firestore/cloud_firestore.dart';

class UserService {
  final db = FirebaseFirestore.instance;

  // Add a restaurant to the user's favorite list, if the restaurant is already in the list, remove it
  Future<void> toggleFavorite(String userId, String restaurantId) async {
    final doc = await db.collection('users').doc(userId).get();
    final data = doc.data();

    List<String> favoriteRestaurants = [];

    if (data != null && data['favorite_restaurants'] != null) {
      favoriteRestaurants = (data['favorite_restaurants'] as List<dynamic>)
          .map((e) => e as String)
          .toList();
    }

    if (favoriteRestaurants.contains(restaurantId)) {
      favoriteRestaurants.remove(restaurantId);
    } else {
      favoriteRestaurants.add(restaurantId);
    }

    await db.collection('users').doc(userId).update({
      'favorite_restaurants': favoriteRestaurants,
    });
  }

  // Get the list of favorite restaurants
  Future<List<String>> getFavoriteRestaurants(String userId) async {
    final doc = await db.collection('users').doc(userId).get();
    final data = doc.data();

    return data!.containsKey("favorite_restaurants")
        ? (data['favorite_restaurants'] as List<dynamic>)
            .map((element) => element.toString())
            .toList()
        : [];
  }

  //Add restaurant to owner
  Future<void> addRestaurantToOwner({
    required String ownerId,
    required String restaurantId,
  }) async {
    await db
        .collection('users')
        .doc(ownerId)
        .update({'restaurant': restaurantId});
  }
}
