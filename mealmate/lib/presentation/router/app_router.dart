import 'package:flutter/material.dart';
import 'package:mealmate/presentation/pages/create_menu_page.dart';
import 'package:mealmate/presentation/pages/create_promotion_page.dart';
import 'package:mealmate/presentation/pages/create_restaurant_page.dart';
import 'package:mealmate/presentation/pages/list_promotions_page.dart';
import 'package:mealmate/presentation/pages/login_register_page.dart';
import 'package:mealmate/presentation/pages/main_page.dart';
import 'package:mealmate/presentation/pages/promotions_options_page.dart';
import 'package:mealmate/presentation/pages/restaurant_detail_page.dart';
import 'package:mealmate/presentation/pages/restaurant_detail_user_page.dart';
import 'package:mealmate/presentation/pages/restaurants_list_page.dart';
import 'package:mealmate/presentation/pages/settings_restaurant.dart';
import 'package:mealmate/presentation/widgets/widget_tree.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => const Main());
      case '/home':
        return MaterialPageRoute(builder: (_) => const WidgetTree());
      case '/list_promotions':
        return MaterialPageRoute(builder: (_) => const PromotionsListView());
      case "/promotions_options":
        return MaterialPageRoute(builder: (_) => const PromotionsOptionsView());
      case '/restaurants':
        return MaterialPageRoute(builder: (_) => const ListRestaurantsView());
      case '/create_promotion':
        return MaterialPageRoute(builder: (_) => const CreatePromotionView());
      case '/create_restaurant':
        return MaterialPageRoute(builder: (_) => const NewRestaurantView());
      case '/login_register':
        return MaterialPageRoute(builder: (_) => const LoginPage());
      case '/settings':
        return MaterialPageRoute(
            builder: (_) => const SettingsRestaurantView());
      case '/menu_creation':
        return MaterialPageRoute(builder: (_) => const MenuCreationView());
      case '/detail_restaurant':
        return MaterialPageRoute(builder: (_) => const RestaurantDetailPage());
      case '/detail_restaurant_user':
        return MaterialPageRoute(
            builder: (_) => const RestaurantDetailUserPage());
      default:
        return null;
    }
  }
}
