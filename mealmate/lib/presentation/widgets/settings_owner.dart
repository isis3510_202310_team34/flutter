import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SettingsOwner extends StatelessWidget {
  final String name;
  final String email;
  const SettingsOwner({super.key, required this.name, required this.email});

  @override
  Widget build(BuildContext context) {
    // A view with two buttoms: Edit Profile and Logout
    return Scaffold(
      appBar: AppBar(
        title: const Text("Settings Owner"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              name,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              email,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/detail_restaurant');
              },
              child: const Text("View Restaurant"),
            ),
            ElevatedButton(
              onPressed: () {
                FirebaseAuth.instance.signOut();
                Navigator.pushNamedAndRemoveUntil(
                    context, '/', (route) => false);
              },
              child: const Text("Logout"),
            ),
          ],
        ),
      ),
    );
  }
}
