// Profile with a profile picture and a name

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:mealmate/business_logic/cubits/user_cubit/user_cubit.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserCubit, UserState>(builder: (_, stateUser) {
      if (stateUser is UserLoaded) {
        return Column(
          children: [
            // Image  that could be save in cache
            // https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Placeholder_view_vector.svg/681px-Placeholder_view_vector.svg.png")
            CachedNetworkImage(
              imageUrl: stateUser.restaurant?.image ??
                  "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Placeholder_view_vector.svg/681px-Placeholder_view_vector.svg.png",
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
              height: 150.0,
              fit: BoxFit.cover,
              cacheManager: CacheManager(Config(
                "customCacheKey",
                stalePeriod: const Duration(minutes: 1),
              )),
              //
            ),

            const SizedBox(height: 10),
            Text(
              stateUser.restaurant?.name ?? "",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10),
            Text(
              "Location: ${stateUser.restaurant?.location?.latitude.toString() ?? ""}, ${stateUser.restaurant?.location?.longitude.toString() ?? ""}",
              style: const TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  // color: Colors.grey,
                  color: Color.fromARGB(255, 61, 61, 61)),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  "/menu_creation",
                  arguments: stateUser.restaurant,
                );
              },
              child: const Text("Create Dishes"),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  "/create_promotion",
                  arguments: stateUser.restaurant,
                );
              },
              child: const Text("Create Promotion"),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                // Navigator.pushNamed(
                //   context,
                //   "/menu_detail",
                //   arguments: stateUser.restaurant,
                // );
              },
              child: const Text("See my menu"),
            ),
          ],
        );
      } else {
        return Container();
      }
    });
  }
}
