import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mealmate/business_logic/cubits/user_cubit/user_cubit.dart';
import 'package:mealmate/data/data_providers/restaurant_api.dart';
import 'package:mealmate/data/models/user_model.dart';
import 'package:mealmate/presentation/pages/create_restaurant_page.dart';
import 'package:mealmate/presentation/pages/login_register_page.dart';
import 'package:mealmate/presentation/pages/restaurants_list_page.dart';

class WidgetTree extends StatefulWidget {
  const WidgetTree({super.key});

  @override
  State<WidgetTree> createState() => _WidgetTreeState();
}

class _WidgetTreeState extends State<WidgetTree> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UserCubit, UserState>(
      listener: (_, stateUser) {},
      builder: (_, stateUser) {
        if (stateUser is UserInitial) {
          print("UserInitial");
          return const LoginPage();
        } else if (stateUser is UserLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (stateUser is UserLoaded) {
          UserModel currentUser = stateUser.user;
          if (currentUser.role == "owner" &&
              stateUser.haveRestaurant == false) {
            print("UserLoaded - owner - no restaurant");
            print(stateUser.haveRestaurant);
            print(stateUser.user.restaurant);
            print(stateUser.restaurant?.name);
            return const NewRestaurantView();
          } else {
            return const ListRestaurantsView();
          }
        } else {
          return const LoginPage();
        }
      },
    );
  }
}
