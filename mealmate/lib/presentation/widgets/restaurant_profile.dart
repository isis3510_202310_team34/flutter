// Profile with a profile picture and a name

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:mealmate/business_logic/cubits/user_cubit/user_cubit.dart';
import 'package:mealmate/data/models/restaurant_model.dart';

// ignore: must_be_immutable
class RestaurantProfile extends StatelessWidget {
  RestaurantProfile({super.key, required this.selectedRestaurant});
  RestaurantModel? selectedRestaurant;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // Image  that could be save in cache
        // https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Placeholder_view_vector.svg/681px-Placeholder_view_vector.svg.png")
        CachedNetworkImage(
          imageUrl: selectedRestaurant?.image ??
              "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Placeholder_view_vector.svg/681px-Placeholder_view_vector.svg.png",
          placeholder: (context, url) => const CircularProgressIndicator(),
          errorWidget: (context, url, error) => const Icon(Icons.error),
          height: 150.0,
          fit: BoxFit.cover,
          cacheManager: CacheManager(Config(
            "customCacheKey",
            stalePeriod: const Duration(minutes: 1),
          )),
          //
        ),

        const SizedBox(height: 10),
        Text(
          selectedRestaurant?.name ?? "",
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 10),
        Text(
          "Location: ${selectedRestaurant?.location?.latitude.toString() ?? "inf"}, ${selectedRestaurant?.location?.longitude.toString() ?? "inf"}",
          style: const TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              // color: Colors.grey,
              color: Color.fromARGB(255, 61, 61, 61)),
        ),
        const SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            // Navigator.pushNamed(
            //   context,
            //   "/menu_creation",
            //   arguments: stateUser.selectedRestaurant,
            // );
          },
          child: const Text("See the menu"),
        ),
      ],
    );
    // return BlocBuilder<UserCubit, UserState>(builder: (_, stateUser) {
    //   if (stateUser is UserLoaded) {
    //     // RestaurantModel? selectedRestaurant = await restaurantApi
    //     //     .getRestaurantById(selectedRestaurantId)
    //     return Column(
    //       children: [
    //         // Image  that could be save in cache
    //         // https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Placeholder_view_vector.svg/681px-Placeholder_view_vector.svg.png")
    //         CachedNetworkImage(
    //           imageUrl: stateUser.selectedRestaurant?.image ??
    //               "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Placeholder_view_vector.svg/681px-Placeholder_view_vector.svg.png",
    //           placeholder: (context, url) => const CircularProgressIndicator(),
    //           errorWidget: (context, url, error) => const Icon(Icons.error),
    //           height: 150.0,
    //           fit: BoxFit.cover,
    //           cacheManager: CacheManager(Config(
    //             "customCacheKey",
    //             stalePeriod: const Duration(minutes: 1),
    //           )),
    //           //
    //         ),

    //         const SizedBox(height: 10),
    //         Text(
    //           stateUser.selectedRestaurant?.name ?? "",
    //           style: const TextStyle(
    //             fontSize: 20,
    //             fontWeight: FontWeight.bold,
    //           ),
    //         ),
    //         const SizedBox(height: 10),
    //         Text(
    //           "Location: ${stateUser.selectedRestaurant?.location?.latitude.toString() ?? ""}, ${stateUser.selectedRestaurant?.location?.longitude.toString() ?? ""}",
    //           style: const TextStyle(
    //               fontSize: 15,
    //               fontWeight: FontWeight.bold,
    //               // color: Colors.grey,
    //               color: Color.fromARGB(255, 61, 61, 61)),
    //         ),
    //         const SizedBox(height: 10),
    //         ElevatedButton(
    //           onPressed: () {
    //             // Navigator.pushNamed(
    //             //   context,
    //             //   "/menu_creation",
    //             //   arguments: stateUser.selectedRestaurant,
    //             // );
    //           },
    //           child: const Text("See the menu"),
    //         ),
    //       ],
    //     );
    //   } else {
    //     return Container();
    //   }
    // });
  }
}
