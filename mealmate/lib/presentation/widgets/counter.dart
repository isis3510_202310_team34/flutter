// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class CounterWidget extends StatefulWidget {
  int counter;
  int min;

  CounterWidget({
    super.key,
    this.counter = 20,
    this.min = 0,
  });

  @override
  State<CounterWidget> createState() {
    return _CounterWidgetState();
  }
}

class _CounterWidgetState extends State<CounterWidget> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    if (_counter > widget.min) {
      setState(() {
        _counter--;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('flutterassets.com'),
      ),
      body: Container(
          alignment: Alignment.topCenter,
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height: 80,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: Colors.grey[200],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  iconSize: 30,
                  icon: const Icon(Icons.remove),
                  onPressed: _decrementCounter,
                ),
                const VerticalDivider(),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.symmetric(vertical: 4),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.grey[100],
                    ),
                    child: Row(
                      children: [
                        const Spacer(),
                        Column(
                          children: [
                            const Text(
                              "Distance",
                              style: TextStyle(fontSize: 20),
                            ),
                            Text(
                              '$_counter',
                              style: const TextStyle(fontSize: 30),
                            ),
                          ],
                        ),
                        const Spacer(),
                      ],
                    ),
                  ),
                ),
                const VerticalDivider(),
                IconButton(
                  iconSize: 30,
                  color: Colors.blue,
                  icon: const Icon(Icons.add),
                  onPressed: _incrementCounter,
                ),
              ],
            ),
          )),
    );
  }
}
