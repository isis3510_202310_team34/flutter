import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mealmate/business_logic/cubits/internet_cubit/internet_cubit.dart';
import 'package:mealmate/business_logic/cubits/user_cubit/user_cubit.dart';
import 'package:mealmate/presentation/widgets/message.dart';
import 'package:mealmate/presentation/widgets/settings_customer.dart';
import 'package:mealmate/presentation/widgets/settings_owner.dart';

class SettingsRestaurantView extends StatelessWidget {
  const SettingsRestaurantView({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<InternetCubit>(
            create: (context) => InternetCubit(connectivity: Connectivity())),
        BlocProvider<UserCubit>(create: (context) => UserCubit(UserInitial())),
      ],
      child: Scaffold(
        body: BlocBuilder<UserCubit, UserState>(builder: (_, stateUser) {
          return BlocBuilder<InternetCubit, InternetState>(
              builder: (_, stateInternet) {
            if (stateUser is UserLoaded) {
              if (stateInternet is InternetConnected) {
                print("Using internet in settings");
              } else {
                print("Using firebase cache in settings");
              }
              if (stateUser.isOwner) {
                return SettingsOwner(
                    name: stateUser.user.name, email: stateUser.user.email);
              } else {
                return SettingsCustomer(
                    name: stateUser.user.name, email: stateUser.user.email);
              }
            } else {
              if (stateInternet is InternetConnected) {
                return const Message(
                    message: "Something went wrong. Please try again later.");
              } else if (stateInternet is InternetDisconnected) {
                return const Message(
                    message: "Please check your internet connection.");
              } else {
                return const Center(child: CircularProgressIndicator());
              }
            }
          });
        }),
      ),
    );
  }
}
