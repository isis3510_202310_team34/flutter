import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mealmate/business_logic/cubits/user_cubit/user_cubit.dart';
import 'package:mealmate/presentation/widgets/message.dart';
import 'package:mealmate/presentation/widgets/profile.dart';
import 'package:mealmate/services/sharedPreferences.dart';

class RestaurantDetailPage extends StatelessWidget {
  const RestaurantDetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Restaurant Detail"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            BlocBuilder<UserCubit, UserState>(
              builder: (_, stateUser) {
                if (stateUser is UserLoaded) {
                  if (stateUser.isOwner && stateUser.haveRestaurant) {
                    return const Profile();
                  } else if (stateUser.isOwner && !stateUser.haveRestaurant) {
                    return const Message(message: "Does not have a restaurant");
                  } else {
                    return const Message(message: "Not an owner");
                  }
                }
                return Container();
              },
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}


// BlocBuilder<UserCubit, UserState>(
    //   builder: (contextUser, stateUser) {
    //     if (stateUser is UserLoaded) {
    //       return Message(message: stateUser.user.restaurant);
    //     }
    //     return Container();
    //   },
    // );