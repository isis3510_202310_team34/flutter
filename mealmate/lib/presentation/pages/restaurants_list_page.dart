import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:mealmate/business_logic/blocs/favorite_restaurants_bloc/favorite_restaurants_bloc.dart';
import 'package:mealmate/business_logic/blocs/list_restaurants_bloc/list_restaurants_bloc.dart';
import 'package:mealmate/business_logic/cubits/counter/counter_cubit.dart';
import 'package:mealmate/business_logic/cubits/gps_cubit/gps_cubit.dart';
import 'package:mealmate/business_logic/cubits/internet_cubit/internet_cubit.dart';
import 'package:mealmate/business_logic/cubits/user_cubit/user_cubit.dart';
import 'package:mealmate/data/models/restaurant_model.dart';
import 'package:mealmate/presentation/widgets/message.dart';
import 'package:mealmate/services/sharedPreferences.dart';

class ListRestaurantsView extends StatefulWidget {
  static const String routeId = "/restaurants2";

  const ListRestaurantsView({super.key});
  @override
  _ListRestaurantsViewState createState() => _ListRestaurantsViewState();
}

class _ListRestaurantsViewState extends State<ListRestaurantsView>
    with WidgetsBindingObserver {
  final Set<RestaurantTags> _selectedFilters = {};

  Widget buildFilterButton(String label, IconData icon, RestaurantTags filter) {
    var isSelected = _selectedFilters.contains(filter);
    return ElevatedButton.icon(
      onPressed: () {
        setState(() {
          if (isSelected) {
            _selectedFilters.remove(filter);
            isSelected = false;
          } else {
            _selectedFilters.add(filter);
            isSelected = true;
          }
        });
      },
      icon: Icon(icon),
      label: Text(label),
      style: ButtonStyle(
        backgroundColor: _selectedFilters.contains(filter)
            ? MaterialStateProperty.all(Colors.blue)
            : MaterialStateProperty.all(Colors.white),
        foregroundColor: _selectedFilters.contains(filter)
            ? MaterialStateProperty.all(Colors.white)
            : MaterialStateProperty.all(Colors.black),
      ),
    );
  }

  AppBar listRestaurantsAppBar(BuildContext context,
      void Function() functionOnPressed, BuildContext contextRestaurants) {
    return AppBar(
      backgroundColor: const Color(0xFFFAFAFF),
      leading: IconButton(
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.black,
        ),
        onPressed: () {
          FirebaseAuth.instance.signOut();
          Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
        },
      ),
      title: Row(
        children: [
          const Text(
            'Restaurants',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(width: 10),
          // Settings button
          IconButton(
            icon: const Icon(
              Icons.settings,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/settings');
            },
          ),
        ],
      ),
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: () {
              showModalBottomSheet(
                  context: context,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    ),
                  ),
                  builder: (BuildContext context) {
                    return Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 24),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            'Filters',
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                          const SizedBox(height: 16),
                          buildFilterButton(
                              RestaurantTags.Vegetarian.toShortString(),
                              const IconData(0xf86c,
                                  fontFamily: 'MaterialIcons'),
                              RestaurantTags.Vegetarian),
                          buildFilterButton(
                              RestaurantTags.Burgers.toShortString(),
                              const IconData(0xe3c2,
                                  fontFamily: 'MaterialIcons'),
                              RestaurantTags.Burgers),
                          buildFilterButton(
                              RestaurantTags.Pizza.toShortString(),
                              Icons.local_pizza,
                              RestaurantTags.Pizza),
                          buildFilterButton(
                              RestaurantTags.Italian.toShortString(),
                              Icons.coffee,
                              RestaurantTags.Italian),
                          // Bloc provider to access the counter cubit
                          BlocProvider(
                            create: (_) => CounterCubit(),
                            child: BlocBuilder<CounterCubit, CounterState>(
                              builder: (contextCounter, stateCounter) {
                                return Column(
                                  children: [
                                    const SizedBox(height: 16),
                                    Row(
                                      children: [
                                        const SizedBox(height: 16),
                                        // Button to increment counter
                                        ElevatedButton(
                                            onPressed: () {
                                              BlocProvider.of<CounterCubit>(
                                                      contextCounter)
                                                  .decrement();
                                            },
                                            child: const Text('-')),
                                        const SizedBox(height: 16),
                                        ElevatedButton(
                                            onPressed: () {},
                                            child: Text(
                                              '${stateCounter.counterValue}',
                                            )),
                                        // Button to decrement counter
                                        ElevatedButton(
                                            onPressed: () {
                                              BlocProvider.of<CounterCubit>(
                                                      contextCounter)
                                                  .increment();
                                            },
                                            child: const Text('+')),
                                        const SizedBox(height: 16),
                                        ElevatedButton(
                                            onPressed: () {
                                              BlocProvider.of<
                                                          ListRestaurantsBloc>(
                                                      contextRestaurants)
                                                  .add(
                                                      FilterRestaurantsByDistanceEvent(
                                                          stateCounter
                                                              .counterValue
                                                              .toDouble()));
                                            },
                                            child:
                                                const Text('Filter by time')),
                                      ],
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                          const SizedBox(height: 16),
                          ElevatedButton(
                            onPressed: functionOnPressed,
                            child: const Text('Apply Filters'),
                          ),
                        ],
                      ),
                    );
                  });
            },
            child: const Center(
              child: Icon(
                Icons.filter_list,
                size: 26.0,
                color: Colors.black,
              ),
            ),
          ),
        ),
      ],
    );
  }

  BottomAppBar listRestaurantsBottomAppBar(BuildContext context) {
    return BottomAppBar(
      color: const Color(0xFFFAFAFF),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              width: 60.0,
              height: 60.0,
              decoration: BoxDecoration(
                color: const Color(0xFF983628),
                border: Border.all(color: Colors.black, width: 2.0),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Center(
                child: IconButton(
                  icon: const Icon(Icons.attach_money_rounded),
                  onPressed: () async {
                    bool containsKeyActualRestaurantId =
                        await SharedPrefUtils.containsKey("actualRestaurantId");
                    if (containsKeyActualRestaurantId) {
                      String actualRestaurantId =
                          await SharedPrefUtils.readPrefStr(
                              "actualRestaurantId");
                      if (actualRestaurantId != "") {
                        Navigator.pushNamed(context, '/promotions_options');
                      } else {
                        Navigator.pushNamed(context, '/list_promotions');
                      }
                    } else {
                      Navigator.pushNamed(context, '/list_promotions');
                    }
                  },
                ),
              ),
            ),
            Container(
              width: 60.0,
              height: 60.0,
              decoration: BoxDecoration(
                color: const Color(0xFF983628),
                border: Border.all(color: Colors.black, width: 2.0),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Center(
                child: IconButton(
                  icon: const Icon(Icons.directions_run, color: Colors.yellow),
                  onPressed: () {
                    // Navigator.pushNamed(context, '/settings');
                  },
                ),
              ),
            ),
            Container(
              width: 60.0,
              height: 60.0,
              decoration: BoxDecoration(
                color: const Color(0xFF983628),
                border: Border.all(color: Colors.black, width: 2.0),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Center(
                child: IconButton(
                  icon: const Icon(Icons.favorite, color: Colors.yellow),
                  onPressed: () {
                    BlocProvider.of<ListRestaurantsBloc>(context)
                        .add(FilterRestaurantsByFavorites());
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: <BlocProvider<dynamic>>[
          BlocProvider<ListRestaurantsBloc>(
              create: (context) => ListRestaurantsBloc(
                  ListRestaurantsLoadingState(), context.read<GpsCubit>())
                ..add(ListRestaurantsLoadEvent())),
          BlocProvider<FavoriteRestaurantsBloc>(
              create: (context) => FavoriteRestaurantsBloc(FavoritesInitial())),
          BlocProvider<InternetCubit>(
              create: (context) => InternetCubit(connectivity: Connectivity())),
        ],
        child: BlocBuilder<InternetCubit, InternetState>(
          builder: (_, stateInternet) {
            return Scaffold(
              body: BlocBuilder<ListRestaurantsBloc, ListRestaurantsState>(
                builder: (listRestaurantsContext, listRestaurantsState) {
                  if (listRestaurantsState is ListRestaurantsLoadingState &&
                      stateInternet is InternetConnected) {
                    // Show a loading indicator while the API call is in progress
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (listRestaurantsState
                          is NoCachedFilterRestaurantsState &&
                      stateInternet is InternetDisconnected) {
                    return BlocBuilder<FavoriteRestaurantsBloc,
                            FavoriteRestaurantsState>(
                        builder: (favRestaurantsContext, favRestaurantsState) {
                      return Scaffold(
                          appBar:
                              listRestaurantsAppBar(listRestaurantsContext, () {
                            BlocProvider.of<ListRestaurantsBloc>(
                                    listRestaurantsContext)
                                .add(FilterRestaurantsEvent(
                                    _selectedFilters.toList()));
                            Navigator.pop(listRestaurantsContext);
                          }, listRestaurantsContext),
                          body:
                              const Message(message: 'No internet connection'),
                          bottomNavigationBar: listRestaurantsBottomAppBar(
                              listRestaurantsContext));
                    });
                  } else if (listRestaurantsState
                      is ListRestaurantsLoadedState) {
                    if (listRestaurantsState.isCachedData) {
                      print("From cache");
                    } else {
                      print("Latest data");
                    }
                    return BlocBuilder<FavoriteRestaurantsBloc,
                            FavoriteRestaurantsState>(
                        builder: (favRestaurantsContext, favRestaurantsState) {
                      ListView listRestaurants = ListView.builder(
                        itemCount: listRestaurantsState.restaurants.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 20),
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: const Color(0xFFECF0F3),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 3,
                                  child: BlocListener<UserCubit, UserState>(
                                    listener: (_, stateUser) {
                                      // UserCubit userCubit =
                                      //     BlocProvider.of<UserCubit>(context);
                                      // userCubit.updateSelectedRestaurant(
                                      //     listRestaurantsState
                                      //         .restaurants[index]
                                      //         .uid as String);
                                    },
                                    child: GestureDetector(
                                      onTap: () {
                                        SharedPrefUtils.saveStr(
                                            "selectedRestaurantId",
                                            listRestaurantsState
                                                .restaurants[index]
                                                .uid as String);
                                        Navigator.pushNamed(
                                          context,
                                          '/detail_restaurant_user',
                                          arguments: listRestaurantsState
                                              .restaurants[index],
                                        );
                                      },
                                      child: CachedNetworkImage(
                                        imageUrl: listRestaurantsState
                                            .restaurants[index].image,
                                        imageBuilder:
                                            (context, imageProvider) =>
                                                Container(
                                          height: 100,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            image: DecorationImage(
                                              image: imageProvider,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        placeholder: (context, url) =>
                                            const CircularProgressIndicator(),
                                        cacheManager: CacheManager(Config(
                                          listRestaurantsState
                                              .restaurants[index].name,
                                          stalePeriod:
                                              const Duration(minutes: 10),
                                        )),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 7,
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          children: [
                                            Text(
                                              listRestaurantsState
                                                  .restaurants[index].name,
                                              style: const TextStyle(
                                                color: Color.fromARGB(
                                                    255, 0, 0, 0),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 14,
                                              ),
                                            ),
                                            const SizedBox(height: 5),
                                            Row(
                                              children: [
                                                const Icon(
                                                  Icons.directions_run,
                                                  size: 20.0,
                                                  color: Colors.black,
                                                ),
                                                Text(
                                                  listRestaurantsState
                                                                  .distances[
                                                              listRestaurantsState
                                                                  .restaurants[
                                                                      index]
                                                                  .uid] !=
                                                          double.infinity
                                                      ? "${listRestaurantsState.distances[listRestaurantsState.restaurants[index].uid]?.toStringAsFixed(2)} min"
                                                      : "No available route",
                                                  style: const TextStyle(
                                                    color: Color.fromARGB(
                                                        255, 0, 0, 0),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 10,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        IconButton(
                                          key: ValueKey(listRestaurantsState
                                              .restaurants[index].uid),
                                          color:
                                              listRestaurantsState.buttonColor[
                                                  listRestaurantsState
                                                      .restaurants[index]
                                                      .uid as String],
                                          icon: Icon(
                                              listRestaurantsState.isFavorite[
                                                      listRestaurantsState
                                                          .restaurants[index]
                                                          .uid as String]!
                                                  ? Icons.favorite
                                                  : Icons.favorite_border,
                                              color: Colors.black),
                                          onPressed: () {
                                            if (listRestaurantsState.isFavorite[
                                                listRestaurantsState
                                                    .restaurants[index]
                                                    .uid as String]!) {
                                              BlocProvider.of<
                                                          FavoriteRestaurantsBloc>(
                                                      favRestaurantsContext)
                                                  .add(RemoveFavoriteEvent(
                                                      listRestaurantsState
                                                          .restaurants[index]));
                                              listRestaurantsState.isFavorite[
                                                  listRestaurantsState
                                                      .restaurants[index]
                                                      .uid as String] = false;
                                            } else {
                                              BlocProvider.of<
                                                          FavoriteRestaurantsBloc>(
                                                      favRestaurantsContext)
                                                  .add(AddFavoriteEvent(
                                                      listRestaurantsState
                                                          .restaurants[index]));

                                              listRestaurantsState.isFavorite[
                                                  listRestaurantsState
                                                      .restaurants[index]
                                                      .uid as String] = true;
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      );
                      return Scaffold(
                          appBar:
                              listRestaurantsAppBar(listRestaurantsContext, () {
                            BlocProvider.of<ListRestaurantsBloc>(
                                    listRestaurantsContext)
                                .add(FilterRestaurantsEvent(
                                    _selectedFilters.toList()));
                            Navigator.pop(listRestaurantsContext);
                          }, listRestaurantsContext),
                          body: listRestaurantsState.restaurants.isNotEmpty
                              ? Container(
                                  color: const Color(0x00fafaff),
                                  child: Container(
                                    margin: const EdgeInsets.all(20),
                                    decoration: BoxDecoration(
                                      color: const Color(0xFFF64740),
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                    child: listRestaurants,
                                  ),
                                )
                              : const Message(message: "No restaurants found"),
                          bottomNavigationBar: listRestaurantsBottomAppBar(
                              listRestaurantsContext));
                    });
                  } else {
                    if (InternetState is InternetDisconnected) {
                      return const Message(message: "No internet connection");
                    } else {
                      return Center(
                        // Retry button
                        child: ElevatedButton(
                          onPressed: () {
                            BlocProvider.of<ListRestaurantsBloc>(
                                    listRestaurantsContext)
                                .add(ListRestaurantsLoadEvent());
                          },
                          child: const Text('Retry'),
                        ),
                      );
                    }
                  }
                },
              ),
            );
          },
        ));
  }
}
