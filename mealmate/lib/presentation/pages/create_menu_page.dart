import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mealmate/business_logic/blocs/create_menu_bloc/create_menu_bloc.dart';
import 'package:mealmate/business_logic/cubits/internet_cubit/internet_cubit.dart';
import 'package:mealmate/data/models/dish_model.dart';
import 'package:mealmate/data/models/user_model.dart';
import 'package:mealmate/data/repositories/user_repository.dart';

class MenuCreationView extends StatefulWidget {
  @override
  const MenuCreationView({super.key});
  @override
  _MenuCreationViewState createState() => _MenuCreationViewState();
}

class _MenuCreationViewState extends State<MenuCreationView> {
  final CurrencyTextInputFormatter _formatter = CurrencyTextInputFormatter();
  final _formKey = GlobalKey<FormState>();
  String _dishName = "";
  String _dishDescription = "";
  int _dishPrice = 0;

  String _imageUrl = "";
  final String _imagePlaceholder =
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Placeholder_view_vector.svg/681px-Placeholder_view_vector.svg.png";

  @override
  void initState() {
    super.initState();
    setState(() {
      _imageUrl = _imagePlaceholder;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<CreateMenuBloc>(
            create: (context) => CreateMenuBloc(CreateDishInitialState()))
      ],
      child: BlocBuilder<CreateMenuBloc, CreateMenuState>(
        builder: (createMenuContext, createMenuState) {
          if (createMenuState is CreateDishLoadingState) {
            // Show a loading indicator while the API call is in progress
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (createMenuState is CreateDishLoadedState) {
            if (createMenuState.otherOne) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/menu_creation', (route) => false);
              });
            } else if (!createMenuState.otherOne) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/restaurants', (route) => false);
              });
            }
            return const Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.check_circle_outline,
                    color: Colors.green,
                    size: 48.0,
                  ),
                  SizedBox(height: 16.0),
                  Text(
                    '¡Creado correctamente!',
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Scaffold(
              appBar: AppBar(
                title: const Text('New Dish'),
                leading: IconButton(
                  icon: const Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    FirebaseAuth.instance.signOut();
                    Navigator.pushNamedAndRemoveUntil(
                        context, '/', (route) => false);
                  },
                ),
              ),
              body: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        TextFormField(
                          decoration: const InputDecoration(
                            labelText: 'Dish name',
                          ),
                          validator: (value) {
                            if (value != null && value.isEmpty) {
                              return 'Please enter a name for your dish';
                            } else if (value != null && value.length > 30) {
                              return 'Please enter a name with less than 30 characters';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _dishName = value!;
                          },
                        ),
                        const SizedBox(height: 16.0),
                        TextFormField(
                          decoration: const InputDecoration(
                            labelText: 'Dish Image',
                          ),
                          validator: (value) {
                            if (value != null && value.isEmpty) {
                              return 'Please enter an image URL for your dish';
                            } else if (value != null &&
                                !Uri.parse(value).isAbsolute) {
                              return 'Please enter a valid URL (https://...)';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _imageUrl = value!;
                          },
                          onChanged: (value) {
                            setState(() {
                              _imageUrl = value;
                              if (_imageUrl == "") {
                                _imageUrl = _imagePlaceholder;
                              }
                            });
                          },
                        ),
                        const SizedBox(height: 16.0),
                        _buildImagePreview(),
                        const SizedBox(height: 16.0),
                        TextFormField(
                          decoration: const InputDecoration(
                            labelText: 'Dish description',
                          ),
                          validator: (value) {
                            if (value != null && value.isEmpty) {
                              return 'Please enter a description for your dish';
                            } else if (value != null && value.length > 100) {
                              return 'Please enter a name with less than 100 characters';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _dishDescription = value!;
                          },
                        ),
                        const SizedBox(height: 16.0),
                        TextFormField(
                          decoration: const InputDecoration(
                            labelText: 'Dish price',
                          ),
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value != null && value.isEmpty) {
                              return 'Please enter a price for your dish';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _dishPrice = int.parse(value!);
                          },
                        ),
                        const SizedBox(height: 16.0),
                        BlocBuilder<InternetCubit, InternetState>(
                            builder: (_, stateInternet) {
                          if (stateInternet is InternetConnected) {
                            return Row(children: [
                              Expanded(
                                child: ElevatedButton(
                                  child: const Text('Create Dish and Continue',
                                      textAlign: TextAlign.center),
                                  onPressed: () async {
                                    if (_formKey.currentState!.validate()) {
                                      _formKey.currentState!.save();
                                      DishModel newDish = DishModel(
                                          name: _dishName,
                                          description: _dishDescription,
                                          price: _dishPrice,
                                          image: _imageUrl);

                                      String currentUserId =
                                          await UserRepository()
                                              .getCurrentUserId();
                                      Map<String, dynamic> currentUserMap =
                                          await UserRepository()
                                              .getUser(currentUserId);

                                      UserModel user = UserModel(
                                          uid: currentUserMap['uid'],
                                          email: currentUserMap['email'],
                                          name: currentUserMap['name'],
                                          role: currentUserMap['role'],
                                          restaurant:
                                              currentUserMap['restaurant'],
                                          phone: currentUserMap['phone']);

                                      BlocProvider.of<CreateMenuBloc>(
                                              createMenuContext)
                                          .add(RegisterNewDishEvent(
                                              newDish, false, user.restaurant));
                                    }
                                  },
                                ),
                              ),
                              const SizedBox(width: 10.0),
                              Expanded(
                                child: ElevatedButton(
                                  child: const Text('Create Dish and one more',
                                      textAlign: TextAlign.center),
                                  onPressed: () async {
                                    if (_formKey.currentState!.validate()) {
                                      _formKey.currentState!.save();
                                      DishModel newDish = DishModel(
                                          name: _dishName,
                                          description: _dishDescription,
                                          price: _dishPrice,
                                          image: _imageUrl);

                                      String currentUserId =
                                          await UserRepository()
                                              .getCurrentUserId();
                                      Map<String, dynamic> currentUserMap =
                                          await UserRepository()
                                              .getUser(currentUserId);

                                      UserModel user = UserModel(
                                          uid: currentUserMap['uid'],
                                          email: currentUserMap['email'],
                                          name: currentUserMap['name'],
                                          role: currentUserMap['role'],
                                          restaurant:
                                              currentUserMap['restaurant'],
                                          phone: currentUserMap['phone']);
                                      BlocProvider.of<CreateMenuBloc>(
                                              createMenuContext as BuildContext)
                                          .add(RegisterNewDishEvent(
                                              newDish, true, user.restaurant));
                                    }
                                  },
                                ),
                              )
                            ]);
                          } else {
                            return ElevatedButton(
                              child: const Text('Create Dish'),
                              onPressed: () async {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text('No internet connection'),
                                    backgroundColor: Colors.red,
                                    duration: Duration(seconds: 3),
                                  ),
                                );
                              },
                            );
                          }
                        }),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildImagePreview() {
    if (_imageUrl == null) {
      return const SizedBox.shrink();
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          'Image Preview:',
          style: Theme.of(context).textTheme.titleMedium,
        ),
        const SizedBox(height: 8.0),
        Image.network(
          _imageUrl,
          height: 150.0,
          fit: BoxFit.cover,
        ),
      ],
    );
  }
}
