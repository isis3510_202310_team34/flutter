import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../business_logic/blocs/promotion_bloc/promotion_bloc.dart';
import '../../data/models/promotion_model.dart';

class PromotionsListView extends StatefulWidget {
  const PromotionsListView({Key? key}) : super(key: key);

  @override
  _PromotionsListViewState createState() => _PromotionsListViewState();
}

class _PromotionsListViewState extends State<PromotionsListView> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<PromotionBloc>(
          create: (context) => PromotionBloc(PromotionInitialState())
            ..add(ListPromotionsEvent()),
        ),
      ],
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Promotions'),
        ),
        body: BlocBuilder<PromotionBloc, PromotionState>(
          builder: (promotionContext, promotionState) {
            if (promotionState is PromotionLoadingState) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (promotionState is NoInternetForPromotionState) {
              return const Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.wifi_off,
                    size: 64,
                    color: Colors.red,
                  ),
                  SizedBox(height: 16),
                  Text(
                    'No internet connection',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ));
            } else if (promotionState is PromotionsListState) {
              final List<PromotionModel> promotions = promotionState.promotions;
              return ListView.builder(
                itemCount: promotions.length,
                itemBuilder: (context, index) {
                  final PromotionModel promotion = promotions[index];
                  return ListTile(
                    title: Text(
                      promotion.dishName,
                      style: const TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    leading: CachedNetworkImage(
                      imageUrl: promotion.image,
                      width: 80.0,
                      height: 80.0,
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          const CircularProgressIndicator(),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Restaurant:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(promotion.restaurantName),
                        const SizedBox(height: 8.0),
                        const Text(
                          'Description:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(promotion.description),
                        const SizedBox(height: 8.0),
                        const Text(
                          'Discount percentage:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text('${promotion.discountPercentage}'),
                        const SizedBox(height: 8.0),
                        Text(
                          'Discount percentage: ${promotion.discountPercentage}',
                        ),
                        const SizedBox(height: 8.0),
                        const Text(
                          'Price Before:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          '\$${promotion.currentlyPrice}',
                          style: const TextStyle(
                            color: Colors.red,
                          ),
                        ),
                        const SizedBox(height: 8.0),
                        const Text(
                          'Price After:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          '\$${promotion.newPrices}',
                          style: const TextStyle(
                            color: Colors.green,
                          ),
                        ),
                      ],
                    ),
                    onTap: () {
                      // Lógica para manejar el evento al hacer clic en una promoción
                    },
                  );
                },
              );
            } else {
              return const Center(
                child: Text('Failed to load promotions'),
              );
            }
          },
        ),
      ),
    );
  }
}
