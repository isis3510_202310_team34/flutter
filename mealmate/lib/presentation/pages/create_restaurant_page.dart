import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mealmate/business_logic/blocs/register_restaurants_bloc/register_restaurant_bloc.dart';
import 'package:mealmate/business_logic/cubits/internet_cubit/internet_cubit.dart';
import 'package:mealmate/data/models/restaurant_model.dart';
import 'package:mealmate/data/repositories/restaurant_repository.dart';
import 'package:mealmate/data/repositories/user_repository.dart';
import 'package:mealmate/services/auth.dart';
import 'package:mealmate/services/user_service.dart';

import '../../services/sharedPreferences.dart';

class NewRestaurantView extends StatefulWidget {
  const NewRestaurantView({super.key});

  @override
  _NewRestaurantViewState createState() => _NewRestaurantViewState();
}

class _NewRestaurantViewState extends State<NewRestaurantView> {
  final String _imagePlaceholder =
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Placeholder_view_vector.svg/681px-Placeholder_view_vector.svg.png";

  String _restaurantName = "";
  String _imageUrl = "";
  final List<String> _tags = [];

  final _formKey = GlobalKey<FormState>();

  RestaurantRepository restaurantService = RestaurantRepository();
  UserService userService = UserService();
  Auth authService = Auth();

  bool isValidImageUrl(String url) {
    if (url.isEmpty) return false;

    Uri? uri = Uri.tryParse(url);

    if (uri == null || uri.scheme.isEmpty || uri.host.isEmpty) {
      return false;
    }

    return true;
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _imageUrl = _imagePlaceholder;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<RegisterRestaurantsBloc>(
            create: (context) =>
                RegisterRestaurantsBloc(RegisterRestaurantInitialState()))
      ],
      child: BlocBuilder<RegisterRestaurantsBloc, RegisterRestaurantState>(
        builder: (registerRestaurantContext, registerRestaurantState) {
          if (registerRestaurantState is RegisterRestaurantLoadingState) {
            // Show a loading indicator while the API call is in progress
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (registerRestaurantState is RegisterRestaurantLoadedState) {
            SharedPrefUtils.saveStr(
                "selectedRestaurantId", registerRestaurantState.restaurantId);
            WidgetsBinding.instance.addPostFrameCallback((_) {
              Navigator.pushNamedAndRemoveUntil(
                  context, '/restaurants', (route) => false);
            });
            return const Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.check_circle_outline,
                    color: Colors.green,
                    size: 48.0,
                  ),
                  SizedBox(height: 16.0),
                  Text(
                    '¡Creado correctamente!',
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Scaffold(
              appBar: AppBar(
                title: const Text('New Restaurant'),
                leading: IconButton(
                  icon: const Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    FirebaseAuth.instance.signOut();
                    Navigator.pushNamedAndRemoveUntil(
                        context, '/', (route) => false);
                  },
                ),
              ),
              body: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        TextFormField(
                          decoration: const InputDecoration(
                            labelText: 'Restaurant Name',
                          ),
                          validator: (value) {
                            if (value != null && value.isEmpty) {
                              return 'Please enter a name for your restaurant';
                            } else if (value != null && value.length > 20) {
                              return 'Please enter a name with less than 20 characters';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _restaurantName = value!;
                          },
                        ),
                        const SizedBox(height: 16.0),
                        TextFormField(
                          decoration: const InputDecoration(
                            labelText: 'Image URL',
                          ),
                          validator: (value) {
                            if (value != null && value.isEmpty) {
                              return 'Please enter an image URL for your restaurant';
                            } else if (value != null &&
                                !Uri.parse(value).isAbsolute) {
                              return 'Please enter a valid URL (https://...)';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _imageUrl = value!;
                          },
                          onChanged: (value) {
                            setState(() {
                              _imageUrl = value;
                              if (_imageUrl == "") {
                                _imageUrl = _imagePlaceholder;
                              }
                            });
                          },
                        ),
                        const SizedBox(height: 16.0),
                        _buildImagePreview(),
                        const SizedBox(height: 16.0),
                        Text(
                          'Tags:',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                        _buildTags(),
                        const SizedBox(height: 16.0),
                        BlocBuilder<InternetCubit, InternetState>(
                            builder: (_, stateInternet) {
                          if (stateInternet is InternetConnected) {
                            return ElevatedButton(
                              child: const Text('Create Restaurant'),
                              onPressed: () async {
                                if (_formKey.currentState!.validate()) {
                                  _formKey.currentState!.save();

                                  List<RestaurantTags> enumTags = [];
                                  for (String string in _tags) {
                                    for (RestaurantTags tag
                                        in RestaurantTags.values) {
                                      if (tag.toString().split('.').last ==
                                          string) {
                                        enumTags.add(tag);
                                        break;
                                      }
                                    }
                                  }

                                  print("enumTags");
                                  print(enumTags.runtimeType);
                                  print(enumTags);

                                  String currentUserId =
                                      await UserRepository().getCurrentUserId();

                                  BlocProvider.of<RegisterRestaurantsBloc>(
                                          registerRestaurantContext)
                                      .add(RegisterNewRestaurantEvent(
                                          _restaurantName,
                                          _imageUrl,
                                          enumTags,
                                          currentUserId));
                                }
                              },
                            );
                          } else {
                            return ElevatedButton(
                              child: const Text('Create Restaurant'),
                              onPressed: () async {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text('No internet connection'),
                                    backgroundColor: Colors.red,
                                    duration: Duration(seconds: 3),
                                  ),
                                );
                              },
                            );
                          }
                        }),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildImagePreview() {
    if (_imageUrl == null) {
      return const SizedBox.shrink();
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          'Image Preview:',
          style: Theme.of(context).textTheme.titleMedium,
        ),
        const SizedBox(height: 8.0),
        Image.network(
          _imageUrl,
          height: 150.0,
          fit: BoxFit.cover,
        ),
      ],
    );
  }

  Widget _buildTags() {
    return Wrap(
      spacing: 8.0,
      children: <Widget>[
        _buildTag('Vegetarian'),
        _buildTag('Burger'),
        _buildTag('Pizza'),
        _buildTag('Italian'),
        _buildTag('Mexican'),
      ],
    );
  }

  Widget _buildTag(String tagName) {
    bool selected = _tags.contains(tagName);

    return FilterChip(
      label: Text(tagName),
      selected: selected,
      onSelected: (value) {
        setState(() {
          if (value) {
            _tags.add(tagName);
          } else {
            _tags.remove(tagName);
          }
        });
      },
    );
  }
}
