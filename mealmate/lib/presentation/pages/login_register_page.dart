import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mealmate/business_logic/cubits/gps_cubit/gps_cubit.dart';
import 'package:mealmate/business_logic/cubits/internet_cubit/internet_cubit.dart';
import 'package:mealmate/data/models/user_model.dart';
import 'package:mealmate/services/auth.dart';
import 'package:mealmate/services/sharedPreferences.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String? errorMessage = '';
  String actualId = '';
  bool isLogin = true;

  final TextEditingController _controllerName = TextEditingController();
  final TextEditingController _controllerRole = TextEditingController();
  final TextEditingController _controllerPhone = TextEditingController();
  final TextEditingController _controllerEmail = TextEditingController();
  final TextEditingController _controllerPassword = TextEditingController();

  Future<void> signInWithEmailAndPassword() async {
    try {
      await Auth().signInWithEmailAndPassword(
        email: _controllerEmail.text,
        password: _controllerPassword.text,
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        setState(() {
          errorMessage = 'No user found for that email.';
        });
      } else if (e.code == 'wrong-password') {
        setState(() {
          errorMessage = 'Wrong password provided for that user.';
        });
      } else {
        setState(() {
          errorMessage = e.message;
        });
      }
      return;
    }
  }

  Future<void> createUser() async {
    try {
      await Auth().createUser(
        name: _controllerName.text,
        email: _controllerEmail.text,
        password: _controllerPassword.text,
        role: _controllerRole.text,
        phone: _controllerPhone.text,
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        setState(() {
          errorMessage = 'The password provided is too weak.';
        });
      } else if (e.code == 'email-already-in-use') {
        setState(() {
          errorMessage = 'The account already exists for that email.';
        });
      }
      return;
    } catch (e) {
      print(e.toString());
      setState(() {
        errorMessage = e.toString();
      });
      return;
    }
    Auth().getUser().then((value) {
      UserModel currentUser = value;
      String currentUserEmail = currentUser.email;
      SharedPrefUtils.saveStr("actualEmail", currentUserEmail);
      String currentUserRole = currentUser.role;
      SharedPrefUtils.saveStr("actualRole", currentUserRole);
      SharedPrefUtils.readPrefStr("actualRole").then((value) {
        print(value);
      });
    });
  }

  Widget _title() {
    return const Text('MealMate');
  }

  // Widget for phone number verify if its a number
  Widget _phoneNumber() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child:
          // Ubication
          Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Text(
            'Phone Number',
            style: TextStyle(
              // Color is black
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
          BlocBuilder<GpsCubit, GpsState>(
            builder: (context, state) {
              if (state is GpsConnected) {
                if (state.position != null) {
                  double latitude = state.position!.latitude;
                  double longitude = state.position!.longitude;
                  return Text(
                    'LAT: $latitude \n LONG: $longitude',
                  );
                } else {
                  return const Text(
                    'No GPS',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  );
                }
              } else {
                return const Text(
                  'No GPS',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                );
              }
            },
          ),
          TextField(
            controller: _controllerPhone,
            keyboardType: TextInputType.number,
            style: const TextStyle(
              color: Colors.black,
            ),
            decoration: const InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
            ),
          ),
        ],
      ),
    );
  }

  Widget _role() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Text(
            'Role',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
          Row(
            children: <Widget>[
              Radio(
                value: 'owner',
                groupValue: _controllerRole.text,
                onChanged: (String? value) {
                  setState(() {
                    _controllerRole.text = value!;
                  });
                },
              ),
              const Text('Owner'),
              Radio(
                value: 'customer',
                groupValue: _controllerRole.text,
                onChanged: (String? value) {
                  setState(() {
                    _controllerRole.text = value!;
                  });
                },
              ),
              const Text('Customer'),
            ],
          ),
        ],
      ),
    );
  }

  Widget _entryField(
    String title,
    TextEditingController controller,
  ) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
          TextField(
            obscureText: title.toLowerCase().contains("password"),
            controller: controller,
            style: const TextStyle(
              color: Colors.black,
            ),
            decoration: const InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
            ),
          ),
        ],
      ),
    );
  }

  Widget _errorMessage() {
    return Text(
      errorMessage ?? '',
      style: const TextStyle(
        color: Colors.red,
      ),
    );
  }

  Widget _submitButton() {
    return BlocBuilder<InternetCubit, InternetState>(
      builder: (_, stateInternet) {
        if (stateInternet is InternetConnected) {
          return ElevatedButton(
            onPressed: () {
              if (isLogin) {
                signInWithEmailAndPassword();
              } else {
                createUser();
              }
            },
            child: Text(
              isLogin ? 'Login' : 'Register',
            ),
          );
        } else {
          return ElevatedButton(
            onPressed: () {
              setState(() {
                errorMessage = "No internet connection";
              });
            },
            child: Text(
              isLogin ? 'Login' : 'Register',
            ),
          );
        }
      },
    );
  }

  Widget _loginOrRegisterButton() {
    return BlocBuilder<InternetCubit, InternetState>(
      builder: (_, stateInternet) {
        if (stateInternet is InternetConnected) {
          errorMessage = "";
          return TextButton(
            onPressed: () {
              setState(() {
                isLogin = !isLogin;
                errorMessage = "";
              });
            },
            child: Text(
              isLogin ? 'Create an account' : 'Have an account? Login',
            ),
          );
        } else {
          return TextButton(
            onPressed: () {
              setState(() {
                isLogin = !isLogin;
                errorMessage = "No internet connection";
              });
            },
            child: Text(
              isLogin ? 'Create an account' : 'Have an account? Login',
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: _title(),
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/images/logo.png',
                  height: 100,
                ),
                if (!isLogin) _entryField('Name', _controllerName),
                if (!isLogin) _role(),
                _entryField('Email', _controllerEmail),
                if (!isLogin) _phoneNumber(),
                _entryField('Password', _controllerPassword),
                _errorMessage(),
                _submitButton(),
                _loginOrRegisterButton(),
              ],
            ),
          ),
        ));
  }
}
