import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../business_logic/blocs/promotion_bloc/promotion_bloc.dart';
import '../../data/models/dish_model.dart';
import '../../services/sharedPreferences.dart';

class CreatePromotionView extends StatefulWidget {
  const CreatePromotionView({super.key});
  static const String routeId = "/create_promotion";
  @override
  _CreatePromotionViewState createState() => _CreatePromotionViewState();
}

class _CreatePromotionViewState extends State<CreatePromotionView> {
  final GlobalKey<FormState> _formKey =
      GlobalKey<FormState>(); // Agrega la clave del formulario
  String selectedDish = "";
  String selectedPromotion = "2X1";
  TextEditingController descriptionController = TextEditingController();
  String selectedDishPrice = "";
  String selectedDishPriceWithPromotion = "";
  List<DishModel> dishList = [];
  String selectedDishImage = "";
  String restaurantName = "";

  List<String> promotionList = [
    '2X1',
  ];

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<PromotionBloc>(
            create: (context) => PromotionBloc(PromotionInitialState())
              ..add(LoadDishesToCreatePromotionEvent()))
      ],
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Create Promotion'),
        ),
        body: BlocBuilder<PromotionBloc, PromotionState>(
          builder: (promotionContext, promotionState) {
            if (promotionState is PromotionLoadingState) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (promotionState is NoInternetForPromotionState) {
              return const Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.wifi_off,
                    size: 64,
                    color: Colors.red,
                  ),
                  SizedBox(height: 16),
                  Text(
                    'No internet connection',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ));
            } else if (promotionState is PromotionCreatedState) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/restaurants', (route) => false);
              });
              return const Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.check_circle_outline,
                      color: Colors.green,
                      size: 48.0,
                    ),
                    SizedBox(height: 16.0),
                    Text(
                      '¡Promoción creada correctamente!',
                      style: TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              );
            } else if (promotionState is PromotionDishesLoadedState) {
              dishList = promotionState.dishes;
              selectedDish = dishList[0].name!;
              selectedDishPrice = dishList
                  .firstWhere((dish) => dish.name == selectedDish)
                  .price
                  .toString();
              selectedDishPriceWithPromotion =
                  calculatePromotionPrice().toString();
              selectedDishImage = dishList
                  .firstWhere((dish) => dish.name == selectedDish)
                  .image!;
              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        DropdownButtonFormField<String>(
                          value: selectedDish,
                          items: dishList.map((DishModel dish) {
                            return DropdownMenuItem<String>(
                              value: dish.name,
                              child: Text(
                                  '${dish.name}'), // Mostrar el nombre del plato junto con el precio normal
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              selectedDish = newValue!;
                              selectedDishPrice = dishList
                                  .firstWhere((dish) => dish.name == newValue)
                                  .price
                                  .toString();
                              selectedDishPriceWithPromotion =
                                  calculatePromotionPrice().toString();
                              selectedDishImage = dishList
                                  .firstWhere(
                                      (dish) => dish.name == selectedDish)
                                  .image!;
                            });
                          },
                          decoration: const InputDecoration(
                            labelText: 'Related dish',
                          ),
                        ),
                        const SizedBox(height: 16.0),
                        Image.network(
                          selectedDishImage, // Mostrar la imagen del plato seleccionado
                          height: 200,
                          width: 200,
                        ),
                        const SizedBox(height: 16.0),

                        Text(
                            'Regular price: $selectedDishPrice'), // Mostrar el precio normal del plato seleccionado
                        Text(
                            'Price with promotion: $selectedDishPriceWithPromotion'), // Mostrar el precio con promoción
                        const SizedBox(height: 16.0),
                        DropdownButtonFormField<String>(
                          value: selectedPromotion,
                          items: promotionList.map((String promotion) {
                            return DropdownMenuItem<String>(
                              value: promotion,
                              child: Text(promotion),
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              selectedPromotion = newValue!;
                            });
                          },
                          decoration: const InputDecoration(
                            labelText: 'Type of promotion',
                          ),
                        ),
                        const SizedBox(height: 16.0),
                        TextFormField(
                          controller: descriptionController,
                          decoration: const InputDecoration(
                            labelText: 'Description',
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter a description';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 16.0),
                        ElevatedButton(
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              // Si la validación del formulario es exitosa
                              String dish = selectedDish ?? '';
                              String promotion = selectedPromotion ?? '';
                              String description = descriptionController.text;
                              String currentPrice = selectedDishPrice ?? "";
                              String image = selectedDishImage ?? "";
                              String priceWithPromotion =
                                  selectedDishPriceWithPromotion ?? "";
                              String restaurantName =
                                  await SharedPrefUtils.readPrefStr(
                                      "actualRestaurantName");

                              BlocProvider.of<PromotionBloc>(promotionContext)
                                  .add(CreatePromotionEvent(
                                      dish,
                                      promotion,
                                      description,
                                      double.parse(currentPrice),
                                      double.parse(priceWithPromotion),
                                      restaurantName,
                                      image));
                            }
                          },
                          child: const Text('Create Promotion'),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }

  double calculatePromotionPrice() {
    double normalPrice = double.parse(selectedDishPrice);
    double promotionPrice =
        normalPrice / 2; // Ejemplo de cálculo para la promoción "2X1".
    return promotionPrice;
  }
}
