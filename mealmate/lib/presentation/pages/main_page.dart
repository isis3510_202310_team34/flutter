import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mealmate/business_logic/cubits/internet_cubit/internet_cubit.dart';
import 'package:mealmate/presentation/widgets/widget_tree.dart';

class Main extends StatelessWidget {
  const Main({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<InternetCubit, InternetState>(
        listener: (context, state) {
          if (state is InternetDisconnected) {
            print("No internet connection");
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text('No internet connection'),
                backgroundColor: Colors.red,
              ),
            );
          } else {
            print("Internet connection");
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text('Internet connected!'),
                backgroundColor: Colors.green,
              ),
            );
          }
        },
        child: const WidgetTree());
  }
}
