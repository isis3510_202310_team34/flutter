import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mealmate/business_logic/cubits/user_cubit/user_cubit.dart';
import 'package:mealmate/data/data_providers/restaurant_api.dart';
import 'package:mealmate/data/models/restaurant_model.dart';
import 'package:mealmate/presentation/widgets/message.dart';
import 'package:mealmate/presentation/widgets/profile.dart';
import 'package:mealmate/presentation/widgets/restaurant_profile.dart';
import 'package:mealmate/services/sharedPreferences.dart';

class RestaurantDetailUserPage extends StatelessWidget {
  const RestaurantDetailUserPage({super.key});

  @override
  Widget build(BuildContext context) {
    RestaurantApi restaurantApi = RestaurantApi();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Restaurant Detail"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FutureBuilder(
              future: SharedPrefUtils.readPrefStr("selectedRestaurantId"),
              builder: (_, restaurantIdSnapshot) {
                if (restaurantIdSnapshot.hasData) {
                  // Get the selected restaurant from restaurantApi
                  return FutureBuilder(
                    future: restaurantApi.getRestaurantById(
                        restaurantIdSnapshot.data?.toString() ?? ""),
                    builder: (_, restaurantSnapshot) {
                      if (restaurantSnapshot.hasData) {
                        RestaurantModel selectedRestaurant =
                            RestaurantModel.fromJson(restaurantSnapshot.data!);
                        return RestaurantProfile(
                            selectedRestaurant: selectedRestaurant);
                      } else {
                        return const Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularProgressIndicator(),
                            Message(message: "Loading restaurant...")
                          ],
                        );
                      }
                    },
                  );
                } else {
                  return const Message(
                      message: "Something went wrong. Please try again later.");
                }
              },
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}


// BlocBuilder<UserCubit, UserState>(
    //   builder: (contextUser, stateUser) {
    //     if (stateUser is UserLoaded) {
    //       return Message(message: stateUser.user.restaurant);
    //     }
    //     return Container();
    //   },
    // );