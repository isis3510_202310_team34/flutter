import 'package:flutter/material.dart';

class PromotionsOptionsView extends StatelessWidget {
  const PromotionsOptionsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Promotions'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/list_promotions');
              },
              child: const Text('Promotions'),
            ),
            const SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/create_promotion');
              },
              child: const Text('Create Promotion'),
            ),
          ],
        ),
      ),
    );
  }
}
