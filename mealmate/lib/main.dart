import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:mealmate/business_logic/cubits/gps_cubit/gps_cubit.dart';
import 'package:mealmate/business_logic/cubits/internet_cubit/internet_cubit.dart';
import 'package:mealmate/business_logic/cubits/user_cubit/user_cubit.dart';
import 'package:mealmate/presentation/router/app_router.dart';
import 'package:cached_network_image/cached_network_image.dart';

Future<void> main() async {
  CachedNetworkImage.logLevel = CacheManagerLogLevel.debug;
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  GetIt.I.registerSingleton<FirebaseAnalytics>(FirebaseAnalytics.instance);
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterFatalError;
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final AppRouter _appRouter = AppRouter();
  final Connectivity _connectivity = Connectivity();

  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<GpsCubit>(
          create: (context) => GpsCubit(),
        ),
        BlocProvider<InternetCubit>(
          create: (context) => InternetCubit(connectivity: _connectivity),
        ),
        BlocProvider<UserCubit>(create: (context) => UserCubit(UserInitial())),
      ],
      child: MaterialApp(
        title: 'MealMate',
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        onGenerateRoute: _appRouter.onGenerateRoute,
      ),
    );
  }
}

// class Main extends StatefulWidget {
//   const Main({
//     super.key,
//     required AppRouter appRouter,
//   }) : _appRouter = appRouter;

//   final AppRouter _appRouter;

//   @override
//   State<Main> createState() => _MainState();
// }

// class _MainState extends State<Main> {
//   @override
//   Widget build(BuildContext context) {
//     return BlocConsumer<InternetCubit, InternetState>(
//       listener: (context, state) {
//         if (state is InternetDisconnected) {
//           print("No internet connection");
//           const SnackBar(
//             content: Text('No internet connection'),
//             backgroundColor: Colors.red,
//           );
//         } else {
//           print("Internet connection");
//           const SnackBar(
//             content: Text('Internet connection'),
//             backgroundColor: Colors.green,
//           );
//         }
//       },
//       builder: (context, state) {
//         return MaterialApp(
//           title: 'MealMate',
//           theme: ThemeData(
//             primarySwatch: Colors.red,
//           ),
//           onGenerateRoute: widget._appRouter.onGenerateRoute,
//         );
//       },
//     );
//   }
// }

// class Home extends StatefulWidget {
//   const Home({Key? key});

//   static const routeId = "/home";

//   @override
//   _HomeState createState() => _HomeState();
// }

// class _HomeState extends State<Home> {
//   final LightSensor _light = LightSensor();
//   var _lux = 0;
//   var dark = false;
//   var alertShown = false;
//   var userAuth = false;

//   @override
//   void initState() {
//     super.initState();
//     LightSensor.lightSensorStream.listen((int lux) {
//       setState(() {
//         _lux = lux;
//         if (_lux < 5 && !alertShown) {
//           _showAlert();
//           alertShown = true;
//         }
//         dark = _lux < 5;
//       });
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         title: 'MealMate',
//         theme: ThemeData(
//           brightness: userAuth ? Brightness.dark : Brightness.light,
//         ),
//         home: const WidgetTree());
//   }

//   void _showAlert() {
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return AlertDialog(
//           title: const Text('Change to Dark Mode?'),
//           content: const Text(
//               'The ambient light is low, do you want to switch to dark mode?'),
//           actions: <Widget>[
//             TextButton(
//               child: const Text('Cancel'),
//               onPressed: () {
//                 Navigator.of(context).pop();
//               },
//             ),
//             TextButton(
//               child: const Text('OK'),
//               onPressed: () {
//                 setState(() {
//                   alertShown = false;
//                   userAuth = true;
//                 });
//                 Navigator.of(context).pop();
//               },
//             ),
//           ],
//         );
//       },
//     );
//   }
// }
