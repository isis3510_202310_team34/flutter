import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

part 'gps_state.dart';

class GpsCubit extends Cubit<GpsState> {
  final Position? position;
  StreamSubscription<Position>? _positionStreamSubscription;
  StreamSubscription<ServiceStatus>? _serviceStatusStreamSubscription;
  LocationPermission permission = LocationPermission.denied;

  GpsCubit({this.position}) : super(GpsLoading()) {
    askForPermission();
  }

  // Ask for permission
  Future<void> askForPermission() async {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      emit(GpsDisconnected());
    } else {
      emit(GpsLoading());
      _serviceStatusStreamSubscription = listenToServiceStatus();
      _positionStreamSubscription = listenToPositionStream();
    }
  }

  StreamSubscription<ServiceStatus> listenToServiceStatus() {
    return Geolocator.getServiceStatusStream().listen((serviceStatus) {
      if (serviceStatus == ServiceStatus.enabled) {
        emitGpsState(position);
      } else {
        emitGpsDisconnected();
      }
    });
  }

  StreamSubscription<Position> listenToPositionStream() {
    return Geolocator.getPositionStream().listen((position) {
      emitGpsState(position);
    });
  }

  void emitGpsState(position) => emit(GpsConnected(
        position: position,
      ));

  void emitGpsDisconnected() => emit(GpsDisconnected());

  // Get distance between two points
  double getDistance(latitude, longitude) {
    double distanceInMeters = Geolocator.distanceBetween(
        position!.latitude, position!.longitude, latitude, longitude);
    return distanceInMeters;
  }

  @override
  Future<void> close() {
    _positionStreamSubscription?.cancel();
    _serviceStatusStreamSubscription?.cancel();
    return super.close();
  }
}
