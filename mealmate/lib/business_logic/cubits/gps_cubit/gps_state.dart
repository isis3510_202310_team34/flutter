part of 'gps_cubit.dart';

@immutable
abstract class GpsState {}

class GpsLoading extends GpsState {}

class GpsConnected extends GpsState {
  final Position? position;

  GpsConnected({
    this.position,
  });
}

class GpsDisconnected extends GpsState {}
