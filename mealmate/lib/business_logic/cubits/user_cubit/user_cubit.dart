import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mealmate/data/data_providers/restaurant_api.dart';
import 'package:mealmate/data/data_providers/user_api.dart';
import 'package:mealmate/data/models/restaurant_model.dart';
import 'package:mealmate/data/models/user_model.dart';
import 'package:mealmate/services/sharedPreferences.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  UserModel? user;
  RestaurantModel? restaurant;
  String? userId;
  bool isOwner = false;
  bool haveRestaurant = false;
  bool logged = false;

  final UserApi userApi = UserApi();
  final RestaurantApi restaurantApi = RestaurantApi();

  StreamSubscription? userStreamSubscription;

  UserCubit(UserInitial userInitial) : super(UserInitial()) {
    userStreamSubscription = listenUserStatus();
  }

  // return FirebaseAuth.instance.authStateChanges().listen((user) {
  StreamSubscription<void> listenUserStatus() {
    return FirebaseAuth.instance.authStateChanges().listen((user) {
      if (user == null) {
        emit(UserInitial());
      } else {
        userId = user.uid;
        SharedPrefUtils.saveStr("actualId", userId!);
        SharedPrefUtils.saveStr("selectedRestaurantId", "");
        updateUser();
      }
    });
  }

  void updateUser() async {
    emit(UserLoading());
    try {
      await userApi.getUser(userId!).then((value) async {
        print("user: ${value.data()}");
        user = UserModel.fromJson(value);
        SharedPrefUtils.saveStr("actualId", userId!);

        haveRestaurant = await restaurantApi
            .restaurantExists(user!.restaurant)
            .then((value) => value);

        print("haveRestaurant: $haveRestaurant");

        if (user!.role == "owner" && haveRestaurant) {
          try {
            await restaurantApi
                .getRestaurantById(user!.restaurant)
                .then((value) {
              print("restaurant: ${value.data()}");
              restaurant = RestaurantModel.fromJson(value);
            });
          } on Exception catch (e) {
            print("ERROR [updatedUser (Restaurant)]: $e");
          }
        } else {
          print("restaurant: NULL");
          restaurant = null;
        }
        print("LOADING");
        emit(UserLoaded(restaurant,
            user: user!,
            isOwner: user!.isOwner,
            haveRestaurant: haveRestaurant));
        print("LOADED");
      });
    } on Exception catch (e) {
      print("ERROR [updateUser]: $e");
    }
  }

  void logout() {
    emit(UserInitial());
  }

  @override
  Future<void> close() {
    userStreamSubscription?.cancel();
    return super.close();
  }
}
