part of 'user_cubit.dart';

/// UserState is an abstract class that will be used to represent the different states of the user.
/// UserInitial is the initial state of the user. Is when no user is logged in.
/// UserLoading is the state when the user is being loaded.
/// UserLoaded is the state when the user is loaded.
/// UserError is the state when an error occurs.

abstract class UserState extends Equatable {
  const UserState();

  @override
  List<Object> get props => [];
}

class UserInitial extends UserState {}

class UserLoading extends UserState {}

class UserLoaded extends UserState {
  final UserModel user;
  final bool isOwner;
  final bool haveRestaurant;
  final RestaurantModel? restaurant;

  const UserLoaded(this.restaurant,
      {required this.user, required this.isOwner, this.haveRestaurant = false});
}
