part of 'counter_cubit.dart';

class CounterState {
  int counterValue;
  int min;

  CounterState({
    this.counterValue = 0,
    this.min = 0,
  });
}
