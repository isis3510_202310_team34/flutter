import 'package:flutter_bloc/flutter_bloc.dart';

part 'counter_state.dart';

class CounterCubit extends Cubit<CounterState> {
  CounterCubit() : super(CounterState(counterValue: 20));

  void increment() => emit(CounterState(counterValue: state.counterValue + 1));

// check if (state.counterValue > state.min) {
  void decrement() {
    if (state.counterValue > state.min) {
      emit(CounterState(counterValue: state.counterValue - 1));
    } else {
      emit(CounterState(counterValue: state.min));
    }
  }
}
