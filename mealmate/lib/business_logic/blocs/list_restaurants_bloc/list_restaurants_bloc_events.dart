part of 'list_restaurants_bloc.dart';

abstract class ListRestaurantsEvent {
  const ListRestaurantsEvent();

  List<Object> get props => [];
}

class ListRestaurantsLoadEvent extends ListRestaurantsEvent {}

class FilterRestaurantsEvent extends ListRestaurantsEvent {
  final List<RestaurantTags> tags;

  FilterRestaurantsEvent(this.tags);
}

class FilterRestaurantsByDistanceEvent extends ListRestaurantsEvent {
  double distance = 20;
  FilterRestaurantsByDistanceEvent(this.distance);
}

class FilterRestaurantsByFavorites extends ListRestaurantsEvent {}
