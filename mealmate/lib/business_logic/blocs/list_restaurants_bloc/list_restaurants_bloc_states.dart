part of 'list_restaurants_bloc.dart';

abstract class ListRestaurantsState {
  const ListRestaurantsState();

  List<Object> get props => [];
}

class ListRestaurantsInitial extends ListRestaurantsState {}

class ListRestaurantsLoadingState extends ListRestaurantsState {}

class NoCachedFilterRestaurantsState extends ListRestaurantsState {}

class ListRestaurantsLoadedState extends ListRestaurantsState {
  final List<RestaurantModel> restaurants;
  final Map<String, bool> isFavorite;
  final Map<String, Color> buttonColor;
  final Map<String, double> distances;
  final bool isCachedData;

  const ListRestaurantsLoadedState(this.restaurants, this.isFavorite,
      this.buttonColor, this.distances, this.isCachedData);

  @override
  List<Object> get props => [restaurants, isFavorite, buttonColor, distances];
}
