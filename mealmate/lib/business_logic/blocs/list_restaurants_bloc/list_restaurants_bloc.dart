import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_it/get_it.dart';
import 'package:mealmate/business_logic/cubits/gps_cubit/gps_cubit.dart';
import 'package:mealmate/business_logic/exceptions/exceptions.dart';
import 'package:mealmate/data/models/restaurant_model.dart';
import 'package:mealmate/data/repositories/restaurant_repository.dart';
import 'package:mealmate/data/repositories/user_repository.dart';
import 'package:mealmate/services/sharedPreferences.dart';

part 'list_restaurants_bloc_events.dart';
part 'list_restaurants_bloc_states.dart';

class ListRestaurantsBloc
    extends Bloc<ListRestaurantsEvent, ListRestaurantsState> {
  final FirebaseAnalytics _analytics = GetIt.instance<FirebaseAnalytics>();
  final UserRepository userRepository = UserRepository();
  bool isFavOn = false;

  // GPS Cubit
  final GpsCubit gpsCubit;
  Position? position;

  StreamSubscription? _positionStreamSubscription;

  ListRestaurantsState get initialState => ListRestaurantsInitial();

  ListRestaurantsBloc(super.initialState, this.gpsCubit) {
    _positionStreamSubscription = gpsCubit.stream.listen((gpsState) {
      if (gpsState is GpsConnected) {
        //print('GpsConnectedStream');
        if (gpsState.position != null) {
          if (position == null) {
            position = gpsState.position;
            add(ListRestaurantsLoadEvent());
          } else {
            //print("Position is not null");
          }
        }
      } else {
        print("Gps is not connected");
      }
    });

    // Filter by favorite
    on<FilterRestaurantsByFavorites>((event, emit) async {
      emit(ListRestaurantsLoadingState());
      try {
        final Map<String, Color> buttonColor = {};
        final Map<String, bool> isFavorite = {};
        final Map<String, double> distances = {};
        final List<RestaurantModel> response = [];

        final restaurants = await RestaurantRepository().getAllRestaurants();

        String currentUserId = await SharedPrefUtils.readPrefStr(('actualId'));

        final userFavoriteRestaurants =
            await userRepository.getFavoriteRestaurants(currentUserId);
        // Delete all restaurants that are not in the distance of 20 minutes
        for (var r in restaurants) {
          {
            if (r.location != null) {
              if (gpsCubit.state is GpsConnected) {
                double distance = double.infinity;
                if (position != null) {
                  distance = Geolocator.distanceBetween(
                          position!.latitude,
                          position!.longitude,
                          r.location!.latitude,
                          r.location!.longitude) /
                      (100 * 5 / 6);
                }
                // With 2 decimals
                distances[r.uid as String] = distance;

                if (isFavOn) {
                  if (r.uid != null) {
                    distances[r.uid as String] = distance;
                    if (userFavoriteRestaurants.contains(r.uid)) {
                      isFavorite[r.uid as String] = true;
                      buttonColor[r.uid as String] = Colors.yellow;
                    } else {
                      isFavorite[r.uid as String] = false;
                      buttonColor[r.uid as String] = Colors.grey;
                    }
                    response.add(r);
                  }
                } else {
                  if (r.uid != null &&
                      userFavoriteRestaurants.contains(r.uid)) {
                    distances[r.uid as String] = distance;
                    if (userFavoriteRestaurants.contains(r.uid)) {
                      isFavorite[r.uid as String] = true;
                      buttonColor[r.uid as String] = Colors.yellow;
                    } else {
                      isFavorite[r.uid as String] = false;
                      buttonColor[r.uid as String] = Colors.grey;
                    }
                    response.add(r);
                  }
                }
              }
            }
          }
        }

        isFavOn = !isFavOn;

        print(response);
        print(isFavorite);
        emit(ListRestaurantsLoadedState(
            response, isFavorite, buttonColor, distances, false));
      } catch (e) {
        print('Error in FilterRestaurantsByFavorite');
        print(e);
      }
    });

    // Filter by distance (returns the restaurants that are in the distance of 20 meters)
    on<FilterRestaurantsByDistanceEvent>((event, emit) async {
      emit(ListRestaurantsLoadingState());
      try {
        final Map<String, Color> buttonColor = {};
        final Map<String, bool> isFavorite = {};
        final Map<String, double> distances = {};
        final List<RestaurantModel> response = [];

        final restaurants = await RestaurantRepository().getAllRestaurants();
        String currentUserId = await SharedPrefUtils.readPrefStr(('actualId'));
        final userFavoriteRestaurants =
            await userRepository.getFavoriteRestaurants(currentUserId);
        // Delete all restaurants that are not in the distance of 20 minutes
        for (var r in restaurants) {
          {
            if (r.location != null) {
              if (gpsCubit.state is GpsConnected) {
                double distance = double.infinity;
                if (position != null) {
                  distance = Geolocator.distanceBetween(
                          position!.latitude,
                          position!.longitude,
                          r.location!.latitude,
                          r.location!.longitude) /
                      (100 * 5 / 6);
                }
                // With 2 decimals
                distances[r.uid as String] = distance;

                if (distance <= event.distance) {
                  distances[r.uid as String] = distance;
                  if (userFavoriteRestaurants.contains(r.uid)) {
                    isFavorite[r.uid as String] = true;
                    buttonColor[r.uid as String] = Colors.yellow;
                  } else {
                    isFavorite[r.uid as String] = false;
                    buttonColor[r.uid as String] = Colors.grey;
                  }
                  response.add(r);
                }
              }
            }
          }
        }
        print(response);
        print(isFavorite);
        emit(ListRestaurantsLoadedState(
            response, isFavorite, buttonColor, distances, false));
      } catch (e) {
        print('Error in FilterRestaurantsByDistanceEvent');
        print(e);
      }
    });

    on<ListRestaurantsLoadEvent>((event, emit) async {
      emit(ListRestaurantsLoadingState());

      try {
        final Map<String, Color> buttonColor = {};
        final Map<String, bool> isFavorite = {};
        final Map<String, double> distances = {};

        final response = await RestaurantRepository().getAllRestaurants();
        print(response);
        String currentUserId = await SharedPrefUtils.readPrefStr(('actualId'));
        print("currentUserId $currentUserId");
        final userFavoriteRestaurants =
            await userRepository.getFavoriteRestaurants(currentUserId);
        print(userFavoriteRestaurants);

        for (var r in response) {
          {
            if (r.location != null) {
              if (gpsCubit.state is GpsConnected) {
                double distance = double.infinity;
                if (position != null) {
                  distance = Geolocator.distanceBetween(
                          position!.latitude,
                          position!.longitude,
                          r.location!.latitude,
                          r.location!.longitude) /
                      (100 * 5 / 6);
                }
                // With 2 decimals
                distances[r.uid as String] = distance;
              } else {
                // if the distance not empty, save  inf
                distances[r.uid as String] = double.infinity;
              }
            } else {
              distances[r.uid as String] = double.infinity;
            }

            if (userFavoriteRestaurants.contains(r.uid)) {
              isFavorite[r.uid as String] = true;
              buttonColor[r.uid as String] = Colors.yellow;
            } else {
              isFavorite[r.uid as String] = false;
              buttonColor[r.uid as String] = const Color(0xFFECF0F3);
            }
          }
        }
        emit(ListRestaurantsLoadedState(
            response, isFavorite, buttonColor, distances, false));
      } catch (e) {
        print(e);
        print('Error in ListRestaurantsLoadEvent');
      }
    });

    on<FilterRestaurantsEvent>((event, emit) async {
      emit(ListRestaurantsLoadingState());
      try {
        try {
          _analytics.logEvent(
            name: "filters_used",
            parameters: {
              "Vegetarian":
                  event.tags.contains(RestaurantTags.Vegetarian) ? 1 : 0,
              "Italian": event.tags.contains(RestaurantTags.Italian) ? 1 : 0,
              "Mexican": event.tags.contains(RestaurantTags.Mexican) ? 1 : 0,
              "Burgers": event.tags.contains(RestaurantTags.Burgers) ? 1 : 0,
              "Pizza": event.tags.contains(RestaurantTags.Pizza) ? 1 : 0
            },
          );
          print("Evento loggeado");
        } catch (e) {
          print("Error en el log");
          print(e);
        }
        final Map<String, Color> buttonColor = {};
        final Map<String, bool> isFavorite = {};
        final Map<String, double> distances = {};

        final response =
            await RestaurantRepository().getFilteredRestaurants(event.tags);
        final String currentUserId = await userRepository.getCurrentUserId();
        final userFavoriteRestaurants =
            await userRepository.getFavoriteRestaurants(currentUserId);

        for (var r in response.restaurants) {
          {
            if (r.location != null) {
              if (gpsCubit.state is GpsConnected) {
                double distance = double.infinity;
                if (position != null) {
                  distance = Geolocator.distanceBetween(
                          position!.latitude,
                          position!.longitude,
                          r.location!.latitude,
                          r.location!.longitude) /
                      (100 * 5 / 6);
                }
                // With 2 decimals
                distances[r.uid as String] = distance;
              } else {
                // if the distance not empty, save N/A
                distances[r.uid as String] = double.infinity;
              }
            } else {
              distances[r.uid as String] = double.infinity;
            }

            if (userFavoriteRestaurants.contains(r.uid)) {
              isFavorite[r.uid as String] = true;
              buttonColor[r.uid as String] = Colors.yellow;
            } else {
              isFavorite[r.uid as String] = false;
              buttonColor[r.uid as String] = const Color(0xFFECF0F3);
            }
          }
        }
        emit(ListRestaurantsLoadedState(response.restaurants, isFavorite,
            buttonColor, distances, response.isCachedData));
      } on NoCachedDataException {
        emit(NoCachedFilterRestaurantsState());
      }
    });
  }

  @override
  Future<void> close() {
    _positionStreamSubscription?.cancel();
    return super.close();
  }
}
