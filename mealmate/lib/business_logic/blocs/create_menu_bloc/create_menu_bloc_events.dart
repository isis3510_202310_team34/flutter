part of 'create_menu_bloc.dart';

abstract class CreateMenuEvent {
  const CreateMenuEvent();

  List<Object> get props => [];
}

class RegisterNewDishEvent extends CreateMenuEvent {
  final DishModel dish;
  final String restaurantId;
  final bool otherOne;

  RegisterNewDishEvent(this.dish, this.otherOne, this.restaurantId);
}
