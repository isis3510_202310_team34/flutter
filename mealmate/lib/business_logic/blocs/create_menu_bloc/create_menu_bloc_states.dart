part of 'create_menu_bloc.dart';

abstract class CreateMenuState {
  const CreateMenuState();

  List<Object> get props => [];
}

class CreateDishInitialState extends CreateMenuState {}

class CreateDishLoadingState extends CreateMenuState {}

class CreateDishLoadedState extends CreateMenuState {
  final String dishId;
  final bool otherOne;

  const CreateDishLoadedState(this.dishId, this.otherOne);

  @override
  List<Object> get props => [dishId, otherOne];
}
