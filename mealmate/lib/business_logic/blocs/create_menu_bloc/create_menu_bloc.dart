import 'package:bloc/bloc.dart';
import 'package:mealmate/data/models/dish_model.dart';
import 'package:mealmate/data/repositories/dish_repository.dart';

part 'create_menu_bloc_events.dart';
part 'create_menu_bloc_states.dart';

class CreateMenuBloc extends Bloc<CreateMenuEvent, CreateMenuState> {
  CreateMenuState get initialState => CreateDishInitialState();

  final DishRepository dishRepository = DishRepository();

  @override
  CreateMenuBloc(super.initialState) {
    on<RegisterNewDishEvent>((event, emit) async {
      emit(CreateDishLoadingState());
      try {
        DishModel dish = event.dish;
        String id = await dishRepository.addDishToRestaurant(
            name: dish.name ?? '',
            price: dish.price ?? 0,
            description: dish.description ?? '',
            image: dish.image ?? '',
            restaurantIdN: event.restaurantId);
        emit(CreateDishLoadedState(id, event.otherOne));
      } catch (e) {
        print("Problema en createMenuBloc");
        print(e);
      }
    });
  }
}
