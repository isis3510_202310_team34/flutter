part of "promotion_bloc.dart";

abstract class PromotionEvent {}

class CreatePromotionEvent extends PromotionEvent {
  final String dishName;
  final String promotionType;
  final String description;
  final double currentPrice;
  final double priceWithPromotion;
  final String restaurantName;
  final String image;

  CreatePromotionEvent(
      this.dishName,
      this.promotionType,
      this.description,
      this.currentPrice,
      this.priceWithPromotion,
      this.restaurantName,
      this.image);
}

class LoadDishesToCreatePromotionEvent extends PromotionEvent {}

class ListPromotionsEvent extends PromotionEvent {}
