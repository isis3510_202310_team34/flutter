part of 'promotion_bloc.dart';

abstract class PromotionState {}

class PromotionInitialState extends PromotionState {}

class PromotionLoadingState extends PromotionState {}

class PromotionCreatedState extends PromotionState {}

class PromotionsListState extends PromotionState {
  final List<PromotionModel> promotions;

  PromotionsListState(this.promotions);
}

class PromotionDishesLoadedState extends PromotionState {
  final List<DishModel> dishes;

  PromotionDishesLoadedState(this.dishes);
}

class NoInternetForPromotionState extends PromotionState {}
