import 'package:bloc/bloc.dart';
import 'package:mealmate/business_logic/exceptions/exceptions.dart';
import 'package:mealmate/data/models/dish_model.dart';
import 'package:mealmate/data/repositories/dish_repository.dart';
import 'package:mealmate/services/sharedPreferences.dart';

import '../../../data/models/promotion_model.dart';
import '../../../data/repositories/promotion_repository.dart';

part 'promotion_bloc_events.dart';
part 'promotion_bloc_states.dart';

class PromotionBloc extends Bloc<PromotionEvent, PromotionState> {
  final PromotionRepository promotionRepository = PromotionRepository();
  final DishRepository dishRepository = DishRepository();

  @override
  PromotionBloc(super.initialState) {
    on<CreatePromotionEvent>((event, emit) async {
      emit(PromotionLoadingState());
      try {
        String currentRestaurantId =
            await SharedPrefUtils.readPrefStr(('actualRestaurantId'));
        await promotionRepository.initializeDatabase();
        await promotionRepository.addPromotionToRestaurant(
            dishName: event.dishName,
            restaurantName: event.restaurantName,
            description: event.description,
            image: event.image,
            currentlyPrice: event.currentPrice,
            newPrices: event.priceWithPromotion,
            discountPercentage: 50,
            restaurantIdN: currentRestaurantId);
        emit(PromotionCreatedState());
      } on NoInternetConnectionException {
        emit(NoInternetForPromotionState());
      } catch (e) {
        print(e);
      }
    });
    on<LoadDishesToCreatePromotionEvent>((event, emit) async {
      emit(PromotionLoadingState());
      try {
        String currentRestaurantId =
            await SharedPrefUtils.readPrefStr(('actualRestaurantId'));
        List<DishModel> dishes = await dishRepository.getDishesByRestaurantId(
            restaurantId: currentRestaurantId);
        emit(PromotionDishesLoadedState(dishes));
      } on NoInternetConnectionException {
        emit(NoInternetForPromotionState());
      } catch (e) {
        print(e);
      }
    });
    on<ListPromotionsEvent>((event, emit) async {
      emit(PromotionLoadingState());
      try {
        String currentRestaurantId =
            await SharedPrefUtils.readPrefStr(('actualRestaurantId'));
        await promotionRepository.initializeDatabase();
        List<PromotionModel> promotions =
            await promotionRepository.getAllPromotions();
        emit(PromotionsListState(promotions));
      } on NoInternetConnectionException {
        emit(NoInternetForPromotionState());
      } catch (e) {
        print(e);
      }
    });
  }
}
