part of 'register_restaurant_bloc.dart';

abstract class RegisterRestaurantState {
  const RegisterRestaurantState();

  List<Object> get props => [];
}

class RegisterRestaurantInitialState extends RegisterRestaurantState {}

class RegisterRestaurantLoadingState extends RegisterRestaurantState {}

class RegisterRestaurantLoadedState extends RegisterRestaurantState {
  final String restaurantId;
  const RegisterRestaurantLoadedState(this.restaurantId);

  @override
  List<Object> get props => [restaurantId];
}
