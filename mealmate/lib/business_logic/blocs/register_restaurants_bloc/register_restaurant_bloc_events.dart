part of 'register_restaurant_bloc.dart';

abstract class RegisterRestaurantEvent {
  const RegisterRestaurantEvent();

  List<Object> get props => [];
}

class RegisterNewRestaurantEvent extends RegisterRestaurantEvent {
  final String restaurantName;
  final String restaurantImage;
  final List<RestaurantTags> restaurantTags;
  final String currentUserId;

  RegisterNewRestaurantEvent(this.restaurantName, this.restaurantImage,
      this.restaurantTags, this.currentUserId);
}
