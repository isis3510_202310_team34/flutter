import 'package:bloc/bloc.dart';
import 'package:mealmate/data/models/restaurant_model.dart';
import 'package:mealmate/data/repositories/restaurant_repository.dart';
import 'package:mealmate/data/repositories/user_repository.dart';

import '../../../services/sharedPreferences.dart';

part 'register_restaurant_bloc_events.dart';
part 'register_restaurant_bloc_states.dart';

class RegisterRestaurantsBloc
    extends Bloc<RegisterRestaurantEvent, RegisterRestaurantState> {
  final UserRepository userRepository = UserRepository();
  final RestaurantRepository restaurantRepository = RestaurantRepository();

  RegisterRestaurantsBloc(super.initialState) {
    on<RegisterNewRestaurantEvent>((event, emit) async {
      emit(RegisterRestaurantLoadingState());

      try {
        String restaurantId = await restaurantRepository.addRestaurant(
            name: event.restaurantName,
            image: event.restaurantImage,
            tags: event.restaurantTags,
            currentUserId: event.currentUserId);

        SharedPrefUtils.saveStr("actualRestaurantId", restaurantId);
        SharedPrefUtils.saveStr("actualRestaurantName", event.restaurantName);

        emit(RegisterRestaurantLoadedState(restaurantId));
      } catch (e) {
        print(e);
      }
    });
  }
}
