import 'package:bloc/bloc.dart';
import 'package:mealmate/data/models/restaurant_model.dart';
import 'package:mealmate/data/repositories/user_repository.dart';
import 'package:mealmate/services/sharedPreferences.dart';

part 'favorite_restaurants_bloc_events.dart';
part 'favorite_restaurants_bloc_states.dart';

class FavoriteRestaurantsBloc
    extends Bloc<FavoriteRestaurantsEvent, FavoriteRestaurantsState> {
  final UserRepository userRepository = UserRepository();

  FavoriteRestaurantsState get initialState => FavoritesInitial();

  @override
  FavoriteRestaurantsBloc(super.initialState) {
    on<AddFavoriteEvent>((event, emit) async {
      emit(FavoriteToggledLoadingState());
      try {
        String currentUserId = await SharedPrefUtils.readPrefStr(('actualId'));
        await userRepository.toggleFavorite(
            currentUserId, event.restaurant.uid.toString());
        emit(FavoriteAddedState(event.restaurant));
      } catch (e) {
        print(e);
      }
    });

    on<RemoveFavoriteEvent>((event, emit) async {
      emit(FavoriteToggledLoadingState());
      try {
        String currentUserId = await SharedPrefUtils.readPrefStr(('actualId'));
        await userRepository.toggleFavorite(
            currentUserId, event.restaurant.uid.toString());
        emit(FavoriteRemovedState(event.restaurant));
      } catch (e) {
        print(e);
      }
    });
  }
}
