part of 'favorite_restaurants_bloc.dart';

abstract class FavoriteRestaurantsEvent {}

class AddFavoriteEvent extends FavoriteRestaurantsEvent {
  final RestaurantModel restaurant;

  AddFavoriteEvent(this.restaurant);
}

class RemoveFavoriteEvent extends FavoriteRestaurantsEvent {
  final RestaurantModel restaurant;

  RemoveFavoriteEvent(this.restaurant);
}
