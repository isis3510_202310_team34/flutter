part of 'favorite_restaurants_bloc.dart';

abstract class FavoriteRestaurantsState {}

class FavoritesInitial extends FavoriteRestaurantsState {}

class FavoriteToggledLoadingState extends FavoriteRestaurantsState {}

class FavoriteAddedState extends FavoriteRestaurantsState {
  final RestaurantModel restaurant;

  FavoriteAddedState(this.restaurant);
}

class FavoriteRemovedState extends FavoriteRestaurantsState {
  final RestaurantModel restaurant;

  FavoriteRemovedState(this.restaurant);
}
