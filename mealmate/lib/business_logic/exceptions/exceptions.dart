// Thrown initially by the data providers indiacting that there is no internet connection
class NoInternetConnectionException implements Exception {}

//Thrown when there is no internet connection and no data stored in cache
class NoCachedDataException implements Exception {}
